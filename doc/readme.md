### 安装方法：

1. 1、修改 config/config.inc.php 文件的 db_config 配置，配置你本地或者服务器的数据库
1. 2、导入 public/Data  下面的最新的sql 文件到数据库（记住日期是最新的）
1. 2、在项目目录下 执行 composer update 更新vendor
1. 3、默认的后台账号密码是 ：jrkadmin  jrkadmin
1. 4、有问题加QQ群 ：498186248  进行讨论
1. 5、本项目码云上的免费开源，请勿在其他平台出卖，一经发现，必究法律责任！
1. 6、本项目最终解释权归Luckyhhy开发者
1. 7、微信模块在本地使用时候会有curl问题解决方法请看
1. 8、https://blog.csdn.net/qq_24562495/article/details/105009495



### 模块管理
1、新建的模块需要验证后台登录的话继承common/controller/basecontroller<br/>
2、api模块采用 jwt 模式<br/>
3、插件模块使用中有问题可以提isseu <br>
3、各种免费的API调用接口封装在  extend/Jrk/Api.php

### 去除 index.php
0、本项目已经修改了如下，放在PHPstudy 下只需要修改第2点即可。

1、修改public目录下的 .htaccess 文件 为如下：


```
<IfModule mod_rewrite.c>
  Options +FollowSymlinks -Multiviews
  RewriteEngine On

  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php?/$1 [QSA,PT,L]
</IfModule>  
```


2、PHPstudy工具给网站添加伪静态如下：（WNMP/LNMP）环境，没有用PHPstudy的自己百度





```
 location / {
    if (!-e $request_filename){
      rewrite  ^(.*)$  /index.php?s=$1  last;   break;
    }
 }
```

