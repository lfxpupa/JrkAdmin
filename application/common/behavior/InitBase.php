<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 10:25
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------

    namespace app\common\behavior;

    class InitBase
    {

        /**
         * 初始化行为入口
         */
        public function run()
        {
            // 初始化常量
            $this->initConst();

        }

        /**
         * 初始化常量
         */
        private function initConst()
        {

            // 初始化分层名称常量
            $this->initLayerConst();

            // 初始化结果常量
            $this->initResultConst();

            // 初始化数据状态常量
            $this->initDataStatusConst();

            // 初始化时间常量
            $this->initTimeConst();

            // 初始化系统常量
            $this->initSystemConst();

            // 初始化API常量
            $this->initApiConst();
        }

        /**
         * 初始化分层名称常量
         */
        private function initLayerConst()
        {
            define('LOGIC_NAME'       , 'logic');
            define('MODEL_NAME'       , 'model');
            define('SERVICE_NAME'     , 'service');
            define('CONTROLLER_NAME'  , 'controller');
            define('VALIDATE_NAME'    , 'validate');
            define('VIEW_NAME'        , 'view');
        }

        /**
         * 初始化结果标识常量
         */
        private function initResultConst()
        {

            define('RESULT_SUCCESS'         , 'success');
            define('RESULT_ERROR'           , 'error');
            define('RESULT_REDIRECT'        , 'redirect');
            define('RESULT_MESSAGE'         , 'message');
            define('RESULT_URL'             , 'url');
            define('RESULT_DATA'            , 'data');
        }

        /**
         * 初始化数据状态常量
         */
        private function initDataStatusConst()
        {
            define('DATA_STATUS_NAME'       , 'status');
            define('DATA_DELETE'            , -1);
            define('DATA_SUCCESS'           , 1);
            define('DATA_ERROR'             , 0);
        }

        /**
         * 初始化API常量
         */
        private function initApiConst()
        {

            define('API_CODE_NAME'          , 'code');
            define('API_MSG_NAME'           , 'msg');
        }

        /**
         * 初始化时间常量
         */
        private function initTimeConst()
        {

            define('TIME_CT_NAME'           , 'create_time');
            define('TIME_UT_NAME'           , 'update_time');
            define('TIME_NOW'               , time());
        }



        /**
         * 初始化系统常量
         */
        private function initSystemConst()
        {
            // 定义网站域名
            define('SITE_URL', 'http://www.luckyhhy.com');
            define('VERSION', 'v1.1');
            define('POST_MAX',rtrim(ini_get('upload_max_filesize'),'M'));
            
            define("SYSTEM_NAME","JrkAdmin"); //定义框架系统名

            define('SYS_APP_NAME'      , config('app_name'));
            define('SYS_HOOK_DIR_NAME'      , 'hook');
            define('SYS_DRIVER_DIR_NAME'    , 'driver');
            define('SYS_COMMON_DIR_NAME'    , 'common');
            define('SYS_STATIC_DIR_NAME'    , 'static');
            define('SYS_ADMINISTRATOR_ID'   , 1);
            define('DS_PROS'            , '/');
            define('DS_CONS'            , '\\');

            define('DB_PREFIX'          , config("database.prefix"));

        }


    }