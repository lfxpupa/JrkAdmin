<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

    use think\facade\Request;
    use think\facade\Route;

    // 定义miss路由
    //Route::miss('check/ApiCheck/miss');

    /* 演示环境禁止操作路由绑定 */
   if (stripos(Request::host(true), 'jrk.jackhhy.cn') !== false) {

        $arr=[
            'admin/menu/addMenu',
            'admin/menu/del',
            'admin/role/addRole',
            'admin/role/del',
            'admin/log/del',
            'admin/index/lockPass',
            'admin/databack/export',
            'admin/databack/del',
            'admin/common/changeStatus',
            'admin/admin/addUser',
            'admin/admin/del',
            'admin/setting/index',
            'admin/setting/pass',
            'admin/setting/safe',
            'admin/setting/emailPost',
            'admin/setting/sitePost',
            'admin/setting/del',
            'admin/article/addArticle',
            'admin/article/del',
            'admin/category/addCategory',
            'admin/category/del',
            'admin/comment/checkComment',
            'admin/comment/del',
            'admin/friendlink/addFriend',
            'admin/friendlink/del',
            'admin/model/addModel',
            'admin/model/del',
            'admin/common/UpImages',
            'admin/common/UpFiles',
            'admin/common/Webploader',
            'admin/common/UpSucaiFile',
            'admin/common/UpThumbImg',
            'admin/common/layeditUpImage',
            'addons/addons/hookOk','addons/addons/hookFalse',
            'addons/addons/hookDel','addons/addons/addHook',
            'addons/addons/saveConfig','addons/addons/enable',
            'addons/addons/disable','addons/addons/uninstall',
            'addons/addons/install','admin/addons/local',
            'wechat/wechat/add','wechat/wechat/state',
            'wechat/wechat/delete','wechat/wechat/layeditUpImage',
            'wechat/wechat/addWeixinMenu','wechat/wechat/menuDel',
            'wechat/wechat/fansAysn','wechat/wechat/fansTagGroup',
            'wechat/wechat/tagState','wechat/wechat/tagAysn',
            'wechat/wechat/tagAdd','wechat/wechat/tagDel',
            'wechat/wechat/messageDel','wechat/wechat/messageReply',
            'wechat/wechat/materialAdd','wechat/wechat/materialEdit',
            'wechat/wechat/materialAysn','wechat/wechat/materialDel',
            'wechat/wechat/materialSend','wechat/wechat/materialPreview',
            'wechat/wechat/qrcodeAdd','wechat/wechat/qrcodeState',
            'wechat/wechat/qrcodeDel','wechat/wechat/replyAdd',
            'wechat/wechat/replyEdit','wechat/wechat/replyDel',
            'wechat/wechat/UpVideo','wechat/wechat/uploads',
            'wechat/wechat/videoUpload','wechat/wechat/voiceUpload',
            'admin/picture/addPicture','admin/picture/removeCategory',
            'admin/album/addAlbum','admin/album/addImages',
            'admin/album/removeCategory','admin/common/UpAlbum',
            'admin/common/upVideos'
        ];

      /*  foreach ($arr as $v){
            Route::post($v, function () {
                return json(['code' => 0, 'msg' => '演示环境禁止操作！']);
            });
        }*/

        $getarr=[
            'admin/common/UpImages',
            'admin/common/UpFiles',
            'admin/common/Webploader',
            'admin/common/UpSucaiFile',
            'admin/common/UpThumbImg',
            'admin/common/layeditUpImage',
            'admin/databack/import',
            'admin/databack/del',
            'addons/addons/install',
            'addons/addons/uninstall',
            'addons/addons/local',
            'wechat/wechat/edit','wechat/wechat/state',
            'wechat/wechat/menuDel'
        ];

      /* foreach ($getarr as $v){
           Route::get($v, function () {
               return json(['code' => 0, 'msg' => '演示环境禁止操作！']);
           });
       }*/


    }


    /*用户自定义前端文章访问路由*/
    Route::get("a-i/:category","index/article/index");

    Route::get("a-l/:category","index/article/lists");

    Route::get("a-s/:category/:id","index/article/show");

