<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/1/9-15:50
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\imple;


    interface Comm
    {

        /**
         * @param array $param
         * @param string $order
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/14
         * @name: getAdminPageData
         * @describe:分页数据
         */
        public function getAdminPageData($param=[],$order='id asc');

        /**
         * @param $data
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: addAndEdit
         * @describe:修改或者添加数据
         */
        public function addAndEdit($data);

        /**
         * @param $id
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: delete
         * @describe:删除数据
         */
        public function del($id);


    }