<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 10:56
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------

namespace app\common\controller;



use Jrk\File;
use think\Controller;
use think\Facade\Cache;
use think\facade\Request;

class BaseController extends Controller{


    // 请求参数
    protected $param;


    protected $domain;

    protected $middleware = ['CheckLogin']; //使用中间件检测登录

    /**
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/14 0014
     * @name: initialize
     * @describe:
     */
    protected function initialize()
    {
        parent::initialize();

        // 初始化请求配置
        $this->initRequestConfig();

        // 初始化全局地址
        $this->initRequestUrl();

        // 初始化配置
        $this->initConfig();

        // 系统应用参数


        // 图片配置
        $this->assign('uploadImgExt', GetSysConfig('upload_image_ext'));
        $this->assign('uploadImgSize', GetSysConfig('upload_image_size'));
        $this->assign('uploadImgMax', GetSysConfig('upload_image_max'));

        // 视频配置
        $this->assign('uploadVideoExt', GetSysConfig('upload_video_ext'));
        $this->assign('uploadVideoSize', GetSysConfig('upload_video_size'));
        $this->assign('uploadVideoMax', GetSysConfig('upload_video_max'));

    }


    /**
     * 初始化配置
     * @return void
     * @author Jackhhy
     * @date 2019-11-14
     */
    public function initConfig()
    {
        // 请求参数
        $this->param = $this->request->param();
        // 分页基础默认值
        defined('LIMIT') or define('LIMIT', isset($this->param['limit']) ? $this->param['limit'] : 20);
        defined('PAGE') or define('PAGE', isset($this->param['page']) ? $this->param['page'] : 1);
    }



    /**
     * 初始化请求配置
     * @return void
     */
    public function initRequestConfig()
    {
        // 定义是否GET请求
        defined('IS_GET') or define('IS_GET', $this->request->isGet());

        // 定义是否POST请求
        defined('IS_POST') or define('IS_POST', $this->request->isPost());

        // 定义是否AJAX请求
        defined('IS_AJAX') or define('IS_AJAX', $this->request->isAjax());

        // 定义是否PAJAX请求
        defined('IS_PJAX') or define('IS_PJAX', $this->request->isPjax());

        // 定义是否PUT请求
        defined('IS_PUT') or define('IS_PUT', $this->request->isPut());

        // 定义是否DELETE请求
        defined('IS_DELETE') or define('IS_DELETE', $this->request->isDelete());

        // 定义是否HEAD请求
        defined('IS_HEAD') or define('IS_HEAD', $this->request->isHead());

        // 定义是否PATCH请求
        defined('IS_PATCH') or define('IS_PATCH', $this->request->isPatch());

        // 定义是否为手机访问
        defined('IS_MOBILE') or define('IS_MOBILE', $this->request->isMobile());


        // 模块名称
        $module_name = $this->request->module();
        // 控制器名称
        $controller_name = $this->request->controller();
        // 操作方法名称
        $action_name = $this->request->action();

        // 定义模块名
        defined('MODULE_NAME') or define('MODULE_NAME', $module_name);
        // 定义控制器名
        defined('CONTROLLER_NAME') or define('CONTROLLER_NAME', $controller_name);
        // 定义操作方法名
        defined('ACTION_NAME') or define('ACTION_NAME', $action_name);

        // 自定义渲染
        $this->assign('module', strtolower($module_name));
        $this->assign('control', strtolower($controller_name));
        $this->assign('act', strtolower($action_name));

        //系统名称和版本号
        $this->assign('sys_version', VERSION);
        $this->assign('sys_name', SYSTEM_NAME);
    }

    /**
     * 初始化全局地址
     * @return void
     */
    public function initRequestUrl()
    {
        //获取当前配置的网站地址
        $this->domain=Request::instance()->domain();
        $this->assign("domain", $this->domain);
    }




    /**
     * @return string
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/2/25
     * @name: session_ids
     * @describe:session
     */
    public function session_ids(){
        return session_id();
    }


    /**
     * @param $type
     * @return bool
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/18
     * @name: cache
     * @describe:缓存更新
     */
    public function cache($type)
    {
        switch ($type) {
            case 'data':
                File::del_dir(ROOT_PATH . 'runtime' . DIRECTORY_SEPARATOR . 'cache');
                Cache::clear();
                break;
            case 'template':
                File::del_dir(ROOT_PATH . 'runtime' . DIRECTORY_SEPARATOR . 'temp');
                break;
            case 'log':
                File::del_dir(ROOT_PATH . 'runtime' . DIRECTORY_SEPARATOR . 'log');
                break;
        }
       return true;
    }

    /**
     * @return string
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: makeToken
     * @describe:生成一个不会重复的字符串
     */
    public function makeToken()
    {
        $str = md5(uniqid(md5(microtime(true)), true)); //
        $str = sha1($str); //加密
        return $str;
    }



    /**
     * @return mixed
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/1/10
     * @name: _empty
     * @describe:空操作
     */
    public function _empty()
    {
        return $this->fetch('admin/@public/404');
    }


}
