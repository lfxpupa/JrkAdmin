<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/3-15:15
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;
    use think\facade\Cache;
    use think\facade\Url;

    class Categorys extends BaseModel implements Comm
    {

        protected $name = "category";


        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: init
         * @describe:
         */
        protected static function init()
        {
            //编辑后可用
            Categorys::event('after_update', function () {
                Cache::tag('Categorys_data')->clear();
            });
            //新增后可用
            Categorys::event('after_insert', function () {
                Cache::tag('Categorys_data')->clear();
            });
            //删除后
            Categorys::event('after_delete', function () {
                Cache::tag('Categorys_data')->clear();
            });
        }

        /**
         * @return \think\model\relation\HasMany
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/3
         * @name: articles
         * @describe: 一对多
         */
        public function articles(){
            return $this->hasMany("articles",'category_id','id');
        }


        /**
         * @return \think\model\relation\HasOne
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: models
         * @describe: 多对一
         */
        public function models(){

            return $this->hasOne("models",'id','model_id');
        }


        /**
         * @param $param
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: add
         * @describe:添加
         */
        public function add($param)
        {
            $res=$this->isUpdate(false)->allowField(true)->save($param);
            if ($param['model_id'] != 4) {
                $fun='lists';
                if ((int)$param['model_id'] == 1 || (int)$param['pid'] == 0) {
                    $fun='index';
                }
                $id=$this->id;
                $this->edit(['id'=>$id,'url'=>$this->getUrlToModelId($id, $param['model_id'], $fun)]);
            }

            if ($res){
                $this->success("操作成功");
            }else{
                $this->error("操作失败");
            }
        }

        /**
         * @param $param
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: edit
         * @describe:编辑
         */
        public function edit($param)
        {
            return $this->isUpdate(true)->allowField(true)->save($param);
        }



        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
           return parent::doAll($data);
        }


        /**
         * @param $id
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: del
         * @describe:
         */
        public function del($id)
        {
            // TODO: Implement del() method.
        }



        /**
         * @param $id
         * @param $model_id
         * @param string $fun
         * @return string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: getUrlToModelId
         * @describe:生成前台可访问url
         */
        public function getUrlToModelId($id, $model_id, $fun='index')
        {
            $model_data=model('Models')->adminGetTableNameToModelId($model_id);
            Url::root('/');
            $url=url('index/'.$model_data['tablename'].'/'.$fun, ['category'=>$id]);
            return $url;
        }



        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: getAdminPageData
         * @describe:显示列表数据
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.

            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(!empty($param['title'])) {
                    $where['name'] = ['like', "%".$param['name']."%"];
                    //$where[] = ['title','like', "%".$param['title']."%"];
                }
            }
            try{
                $data = self::with("models")->where(new Where($where))->order($order)->select()->toArray();
                $count =self::where(new Where($where))->count("id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }
        }



    }