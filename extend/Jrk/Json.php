<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/1/3-16:13
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace Jrk;


    class Json
    {

        private static $SUCCESSFUL_DEFAULT_MSG = 'ok';

        private static $FAIL_DEFAULT_MSG = 'no';

        /**
         * @param $code
         * @param StringService $msg
         * @param array $data
         * @param int $count
         * @author: hhygyl
         * @name: result
         * @describe:以json格式返回数据
         */
        public static function result($code,$msg='',$data=[],$count=0)
        {
            exit(json_encode(compact('code','msg','data','count')));
        }


        /**
         * @param int $count
         * @param array $data
         * @param StringService $msg
         * @author: hhygyl
         * @name: successlayui
         * @describe:layui 数据返回
         */
        public static function successlayui($count=0,$data=[],$msg='')
        {
            if(is_array($count)){
                if(isset($count['data'])) $data=$count['data'];
                if(isset($count['count'])) $count=$count['count'];
            }
            if(false == is_string($msg)){
                $data = $msg;
                $msg = self::$SUCCESSFUL_DEFAULT_MSG;
            }
            return self::result(0,$msg,$data,$count);
        }



        /**
         * @param StringService $msg
         * @param array $data
         * @param int $status
         * @author: hhygyl
         * @name: successful
         * @describe:返回成功数据
         */
        public static function successful($msg = 'ok',$data=[],$status=200)
        {
            if(false == is_string($msg)){
                $data = $msg;
                $msg = self::$SUCCESSFUL_DEFAULT_MSG;
            }
            return self::result($status,$msg,$data);
        }


        /**
         * @param $status
         * @param $msg
         * @param array $result
         * @author: hhygyl
         * @name: status
         * @describe:返回状态
         */
        public static function status($status,$msg,$result = [])
        {
            $status = strtoupper($status);
            if(true == is_array($msg)){
                $result = $msg;
                $msg = self::$SUCCESSFUL_DEFAULT_MSG;
            }
            return self::result(200,$msg,compact('status','result'));
        }


        /**
         * @param $msg
         * @param array $data
         * @author: hhygyl
         * @name: fail
         * @describe:失败
         */
        public static function fail($msg,$data=[])
        {
            if(true == is_array($msg)){
                $data = $msg;
                $msg = self::$FAIL_DEFAULT_MSG;
            }
            return self::result(400,$msg,$data);
        }



        /**
         * @param $msg
         * @param array $data
         * @author: hhygyl
         * @name: success
         * @describe:成功
         */
        public static function success($msg,$data=[])
        {
            if(true == is_array($msg)){
                $data = $msg;
                $msg = self::$SUCCESSFUL_DEFAULT_MSG;
            }
            return self::result(200,$msg,$data);
        }


    }