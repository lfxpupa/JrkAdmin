<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/5-14:21
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;
    use think\facade\Cache;
    use think\facade\Url;
    use think\model\concern\SoftDelete;

    class Pictures extends BaseModel implements Comm
    {

        protected $name ="picture";

        use SoftDelete;

        protected $deleteTime = 'delete_time';
        protected $defaultSoftDelete = 0;

        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/5
         * @name: init
         * @describe:
         */
        protected static function init()
        {
            //编辑后可用
            Pictures::event('after_update', function () {
                Cache::tag('Pictures_data')->clear();
            });
            //新增后可用
            Pictures::event('after_insert', function () {
                Cache::tag('Pictures_data')->clear();
            });
            //删除后
            Pictures::event('after_delete', function () {
                Cache::tag('Pictures_data')->clear();
            });
        }

        /**
         * @return \think\model\relation\HasOne
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/3
         * @name: category
         * @describe:
         */
        public function category(){
            return  $this->hasOne("Categorys",'id','category_id')->bind("name");
        }


        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/5
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['title']) && $param['title'] !='') {
                    $where['title'] =['like', "%".$param['title']."%"];
                }

                if(isset($param['category_id']) && $param['category_id'] !=0) {
                    $where['category_id'] =$param['category_id'];
                }
            }

            try{
                $data = self::with("category")->where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
                $count =self::where(new Where($where))->count("id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }
        }



        /**
         * @param $data
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/5
         * @name: add
         * @describe:添加
         */
        public function add($data)
        {
            // TODO: Implement addAndEdit() method.
            $res=$this->isUpdate(false)->allowField(true)->save($data);
            $id=$this->id;
            Url::root("/");
            $url=url('index/picture/show',['id'=>$id]);
            $this->edit(['url'=>$url,'id'=>$id]);

            if ($res){
                $this->success("操作成功");
            }else{
                $this->error("操作失败");
            }

        }


        /**
         * @param $params
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: edit
         * @describe:编辑
         */
        public function edit($params){
            return $this->isUpdate(true)->allowField(true)->save($params);
        }



        /**
         * @param $ids
         * @param $category_id
         * @return bool|\think\Collection
         * @throws \Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/5
         * @name: removeCategory
         * @describe:
         */
        public function removeCategory($ids,$category_id){
            if(is_array($ids)){
                $up_arr=[];
                foreach ($ids as $k=>$v){
                    $up_arr[$k]['id']=$v;
                    $up_arr[$k]['category_id']=$category_id;
                }
                $res= $this->isUpdate(true)->allowField(true)->saveAll($up_arr);
                $res?$this->success("操作成功"):$this->error("操作失败");
            }
            $this->error("id参数无效");
            return false;
        }


        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/5
         * @name: addAndEdit
         * @describe:编辑添加
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }



        /**
         * @param array $where
         * @return array|bool
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: getOne
         * @describe: 查询单个数据
         */
        public function getOne($where = [])
        {
            if (empty($where)){
                return false;
            }
            return self::where($where)->find();
        }



        /**
         * @param $id
         * @return bool|mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/5
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            // TODO: Implement del() method.
            return parent::JrkDelete($id);
        }






    }