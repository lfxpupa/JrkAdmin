<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/1/9-9:19
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace Jrk;
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    // Load Composer's autoloader
    require VENDOR_PATH.'autoload.php';

    class Email
    {
        /**
         * 单例对象
         */
        protected static $instance;

        /**
         * phpmailer对象
         */
        protected $mail = [];

        /**
         * 错误内容
         */
        protected $_error = '';

        /**
         * 默认配置
         */
        public $options = [
            'charset' => 'utf-8', //编码格式
            'debug'   => 0, //调式模式
        ];

        /**
         * @param array $options
         * @return static
         * @throws \think\Exception
         * @throws \think\exception\PDOException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/14 0014
         * @name: instance
         * @describe:
         */
        public static function instance($options = [])
        {
            if (is_null(self::$instance)) {
                self::$instance = new static($options);
            }

            return self::$instance;
        }


        /**
         * Email constructor.
         * @param array $options
         * @throws \think\Exception
         * @throws \think\exception\PDOException
         */
        public function __construct($options = [])
        {

            $this->options = array_merge($this->options, $options);

            $securArr = [1 => 'tls', 2 => 'ssl'];

            $this->mail = new PHPMailer(true);
            $this->mail->CharSet = $this->options['charset'];
            $this->mail->SMTPDebug = $this->options['debug'];
            $this->mail->isSMTP();
            $this->mail->SMTPAuth = true;
            $this->mail->Host = GetSysConfig('mail_smtp_host');
            $this->mail->Username = GetSysConfig('mail_smtp_user');
            $this->mail->Password = GetSysConfig('mail_smtp_pass');
            $this->mail->SMTPSecure = isset($securArr[GetSysConfig('mail_verify_type')]) ? $securArr[GetSysConfig('mail_verify_type')] : '';
            $this->mail->Port = GetSysConfig('mail_smtp_port');

            //设置发件人
            $this->from(GetSysConfig('mail_smtp_user'), GetSysConfig('mail_from_name'));
        }


        /**
         * @param $subject
         * @return $this
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: subject
         * @describe:设置邮件主题
         */
        public function subject($subject)
        {
            $this->options['subject'] = $subject;
            return $this;
        }


        /**
         * @param $file
         * @return $this
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: addAttachment
         * @describe:添加附件
         */
        public function attachment($file){
            $this->options['attachment'] = $file;
            return $this;
        }

        /**
         * @param $email
         * @param string $name
         * @return $this
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: from
         * @describe:设置发件人
         */
        public function from($email, $name = '')
        {
            $this->options['from'] = $email;
            $this->options['from_name'] = $name;
            return $this;
        }

        /**
         * @param $email
         * @param string $name
         * @return $this
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: to
         * @describe:设置收件人
         */
        public function to($email, $name = '')
        {
            $this->options['to'] = $email;
            $this->options['to_name'] = $name;
            return $this;
        }



        /**
         * @param $body
         * @param bool $ishtml
         * @return $this
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: message
         * @describe:设置邮件正文
         */
        public function body($body, $ishtml = true)
        {
            $this->options['body'] = $body;
            $this->options['ishtml'] = $ishtml;
            return $this;
        }

        /**
         * @return string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: getError
         * @describe:获取最后产生的错误
         */
        public function getError()
        {
            return $this->_error;
        }

        /**
         * @param $error
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: setError
         * @describe:设置错误
         */
        protected function setError($error)
        {
            $this->_error = $error;
        }


        /**
         * @return bool
         * @throws Exception
         * @throws \think\Exception
         * @throws \think\exception\PDOException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/14 0014
         * @name: send
         * @describe: 发送邮件
         */
        public function send()
        {
            $result = false;
            switch (GetSysConfig('mail_type')) {
                case 1:
                    //使用phpmailer发送
                    $this->mail->setFrom($this->options['from'], $this->options['from_name']);
                    $this->mail->addAddress($this->options['to'], $this->options['to_name']);
                    if (isset($this->options['attachment'])){
                        $this->mail->addAttachment($this->options['attachment']);
                    }
                    $this->mail->Subject = $this->options['subject'];
                    if ($this->options['ishtml']) {
                        $this->mail->msgHTML($this->options['body']);
                    } else {
                        $this->mail->Body = $this->options['body'];
                    }
                    try {
                        $result = $this->mail->send();
                    } catch (\Exception $e) {
                        $this->setError($e->getMessage());
                    }

                    $this->setError($result ? '' : $this->mail->ErrorInfo);
                    break;
                case 2:
                    //使用mail方法发送邮件
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= "Content-type: text/html; charset=" . $this->options['charset'] . "\r\n";
                    $headers .= "To: {$this->options['to_name']} <{$this->options['to']}>\r\n"; //收件人
                    $headers .= "From: {$this->options['from_name']} <{$this->options['from']}>\r\n"; //发件人
                    $result = mail($this->options['to'], $this->options['subject'], $this->options['body'], $headers);
                    $this->setError($result ? '' : error_get_last()['message']);
                    break;
                default:
                    //邮件功能已关闭
                    $this->setError('Mail already closed');
                    break;
            }
            return $result;
        }


    }