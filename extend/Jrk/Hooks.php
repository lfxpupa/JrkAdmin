<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/1/3-16:13
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------


    namespace Jrk;


    use think\facade\Hook;
    use think\Loader;

    class Hooks
    {
        /**
         * @param $tag
         * @param $params
         * @param null $extra
         * @param bool $once
         * @param null $behavior
         * @return mixed
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: resultListen
         * @describe:监听有返回结果的行为
         */
        public static function resultListen($tag, $params, $extra = null, $once = false,$behavior = null)
        {
            self::beforeListen($tag,$params,$extra,false,$behavior);
            return self::listen($tag,$params,$extra,$once,$behavior);
        }

        /**
         * @param $tag
         * @param $params
         * @param null $extra
         * @param bool $once
         * @param null $behavior
         * @return mixed
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: afterListen
         * @describe:监听后置行为
         */
        public static function afterListen($tag, $params, $extra = null, $once = false, $behavior = null)
        {
            try{
                return self::listen($tag.'_after',$params,$extra,$once,$behavior);
            }catch (\Exception $e){}
        }


        /**
         * @param $tag
         * @param $params
         * @param null $extra
         * @param bool $once
         * @param null $behavior
         * @return mixed
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: beforeListen
         * @describe:监听前置行为
         */
        public static function beforeListen($tag,$params,$extra = null, $once = false, $behavior = null)
        {
            try{
                return self::listen($tag.'_before',$params,$extra,$once,$behavior);
            }catch (\Exception $e){}
        }



        /**
         * @param $tag
         * @param $params
         * @param null $extra
         * @param bool $once
         * @param null $behavior
         * @return mixed
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: listen
         * @describe:监听行为
         */
        public static function listen($tag, $params, $extra = null, $once = false, $behavior = null)
        {
            if($behavior && method_exists($behavior,Loader::parseName($tag,1,false))) self::add($tag,$behavior);
            return Hook::listen($tag,$params,$extra,$once);
        }

        /**
         * @param $tag
         * @param $behavior
         * @param bool $first
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: addBefore
         * @describe:添加前置行为
         */
        public static function addBefore($tag, $behavior, $first = false)
        {
            self::add($tag.'_before',$behavior,$first);
        }

        /**
         * @param $tag
         * @param $behavior
         * @param bool $first
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: addAfter
         * @describe:添加后置行为
         */
        public static function addAfter($tag, $behavior, $first = false)
        {
            self::add($tag.'_after',$behavior,$first);
        }


        /**
         * @param $tag
         * @param $behavior
         * @param bool $first
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: add
         * @describe:添加行为
         */
        public static function add($tag, $behavior, $first = false)
        {
            Hook::add($tag,$behavior,$first);
        }

    }