<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/4-15:25
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;

    class Comments extends BaseModel implements Comm
    {

        protected $name = "comment";


        /**
         * @return \think\model\relation\HasOne
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: article
         * @describe:
         */
        public function article(){
            return $this->hasOne("Articles","id",'a_id')->bind("title");
        }


        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
        }


        /**
         * @param $id
         * @return bool|mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            // TODO: Implement del() method.
            return parent::JrkDelete($id);
        }


        /**
         * @param $ids
         * @return int
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: checkComm
         * @describe:审核数据
         */
        public function checkComm($ids){
            if (!is_array($ids)){
                $ids=@explode(",",$ids);
            }
            $res=self::where("id","in",$ids)->setField("is_ok",1);

            return $res;
        }



        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/4
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id desc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['uid']) && $param['uid'] !=0) {
                    $where['a.uid'] =$param['a.uid'];
                }

                if(isset($param['a_id']) && $param['a_id'] !=0) {
                    $where['a.a_id'] =$param['a.a_id'];
                }
            }

            try{
                $field = ('a.*,t.title,u.username');
                $data = $this->alias("a")
                    ->join("users u","a.uid=u.id","LEFT")
                    ->join("article t","a.a_id=t.id","LEFT")
                    ->where(new Where($where))->order($order)->field($field)->page(PAGE)
                    ->limit(LIMIT)->select()->toArray();

                $count =self::where(new Where($where))->count("id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }

        }



    }