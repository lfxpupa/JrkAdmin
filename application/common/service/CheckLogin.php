<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/9-10:23
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\common\service;


    use app\common\base\Service;

    class CheckLogin extends Service
    {

        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/9
         * @name: isLogin
         * @describe:检测登录
         */
        public function isLogin(){

            $this->app->session->get(config('app.user_info_session')) ? true :false;
        }




    }