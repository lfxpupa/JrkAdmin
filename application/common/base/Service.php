<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/9-10:08
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\common\base;

    use think\App;
    use think\Container;
    use think\Request;


    /**
     * Class LoginAbstract
     * @package app\common\base
     */
    abstract class Service
    {
        /**
         * 当前实例应用
         * @var App
         */
        protected $app;

        /**
         * 当前请求对象
         * @var \think\Request
         */
        protected $request;

        /**
         * Service constructor.
         * @param App $app
         * @param Request $request
         */
        public function __construct(App $app, Request $request)
        {
            $this->app = $app;
            $this->request = $request;
            $this->initialize();
        }

        /**
         * 初始化服务
         * @return $this
         */
        protected function initialize()
        {
            return $this;
        }

        /**
         * 静态实例对象
         * @return static
         */
        public static function instance()
        {
            return Container::getInstance()->make(static::class);
        }


    }