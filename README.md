<p  align="center">
<img src="https://s1.ax1x.com/2020/04/03/GNlrKU.th.jpg"  /> 
</p>
<h1 align="center"> JrkAdmin v1（TP5.1版本） —— 你值得信赖的后端开发框架</h1> 
<p align="center">
<img src="https://gitee.com/luckygyl/JrkAdmin/badge/star.svg?theme=dark"  /> 
<img src="https://gitee.com/luckygyl/JrkAdmin/badge/fork.svg?theme=dark"  /> 
<a href="http://www.php.net/" target="_blank">
<img src="https://img.shields.io/badge/php-%3E%3D5.6-8892BF.svg"  /> 
</a>

</p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

## 项目介绍
JrkAdmin是一款基于ThinkPHP5.1 + layui2.5.6 + ECharts + Mysql开发的后台管理框架。<br>
集成了一般应用所必须的基础性功能，为开发者节省大量的时间。 <br>
是一款快速、高效、便捷、灵活的应用开发框架。 <br>
项目不定时更新，可以 点击 watch 时刻关注本项目动态！


### 主要功能
  1. 模块化：全新的架构和模块化的开发机制，便于灵活扩展和二次开发。</br>
  2. 集成了一般应用所必须的基础性功能，为开发者节省大量的时间。 </br>
  3. 权限认证,api,微信模块采用easywechat最新的4.*版本,易扩展。 <br >
  4. 代码简洁，易扩展，注释明确，易上手，适合新老phper。<br>
  5. 遵循PSR-2命名规范和PSR-4自动加载规范。
  6. queue定时任务，百度，熊掌号平台推送。

### 安装  

使用git安装
~~~
git clone https://gitee.com/luckygyl/JrkAdmin.git
~~~

加载依赖(切换到项目目录下)
~~~
composer update 或者  composer install
~~~

### 环境
推荐使用 Nginx + php7.2 + mysql 

### 浏览器
推荐使用 谷歌，百分，火狐 浏览器

### 导航栏目

 [安装教程](/doc/readme.md)
 | [官网地址](http://www.luckyhhy.cn)
 | [TP5开发手册](https://www.kancloud.cn/manual/thinkphp5_1/content)
 | [服务器](https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=dligum2z)

- - -

### :tw-1f427: QQ交流群
 JrkAdminQQ交流群: 498186248 <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=9ad90a1a2a7cd611cc3343b9b8f59a8ab2a8bbffa2bed243344c41824ebc7f35"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="JrkAdmin" title="JrkAdmin"></a>

####   :fire:  演示站      
[演示地址](http://jrk.jackhhy.cn/)  <br>
用户名密码： 进QQ群 <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=9ad90a1a2a7cd611cc3343b9b8f59a8ab2a8bbffa2bed243344c41824ebc7f35"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="JrkAdmin" title="JrkAdmin"></a><br>
演示地址代码不一定同步更新！
 
###   :tw-1f50a: 开源版使用须知
1.允许用于个人学习、毕业设计、教学案例、公益事业;

2.如果商用必须保留版权信息，请自觉遵守。

3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。

4.版权所有Copyright © 2019-2022 by LuckyHHY (http://www.luckhhy.cn) All rights reserved。

### 更新日志
更新日志在[CHANGELOG.md](/CHANGELOG.md)文件


### 项目截图
![输入图片说明](https://ae01.alicdn.com/kf/H1271761a2cf54ce0b9abcd3c11b2340bu.png "QQ截图20200301144349.png")
![输入图片说明](https://ae01.alicdn.com/kf/H38da31eb07e74637a75adf546c9af903J.png "QQ截图20200301144349.png")
![输入图片说明](https://ae01.alicdn.com/kf/H0a7c8251c3124cb8afad867887b8ef2eD.png "QQ截图20200301144349.png")
![输入图片说明](https://ae01.alicdn.com/kf/Hf7a560f5887041b1b5318c4736800168o.png "QQ截图20200301144349.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0228/095535_8d7dac8e_1513275.png "QQ截图20200228095204.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/151138_18a98978_1513275.png "QQ截图20200301144349.png")



### 特别鸣谢

1. [ok-admin](https://gitee.com/bobi1234/ok-admin/tree/v2.0/)
2. [layui](http://www.layui.com)
3. [thinkphp5.1](http://www.thinkphp.cn)


### 往期项目

1. [图标选择器](https://gitee.com/luckygyl/iconFonts)
2. [luckyblog博客项目](https://gitee.com/luckygyl/luckyblog)