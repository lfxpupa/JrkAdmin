<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/19-10:32
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\wechat\validate;


    use think\Validate;

    class WxAccount extends Validate
    {
        protected $rule = [
            'wxname|token' => [
                'require' => 'require',
                'max'     => '255',
                'unique'  => 'wx_account',
            ],
            'app_id|app_id' => [
                'require' => 'require',
                'max'     => '255',
                'unique'  => 'wx_account',
            ],
            'app_secret|APP_SECRET' => [
                'require' => 'require',
                'max'     => '255',
                'unique'  => 'wx_account',
            ],
            'origin_id|原始id' => [
                'require' => 'require',
                'max'     => '255',
                'unique'  => 'wx_account',
            ],
            'w_token|w_token' => [
                'require' => 'require',
                'max'     => '255',
            ],
            'type|类型' => [
                'require' => 'require',
                'max'     => '2',
            ],
            'status|状态' => [
                'require' => 'require',
                'max'     => '1',
            ],

        ];

    }