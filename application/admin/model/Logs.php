<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 14:38
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------
    namespace app\admin\model;

    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use Jrk\Trees;
    use Jrk\Util;
    use think\Db;
    use think\Exception;

    class Logs extends BaseModel implements Comm
    {

        // 设置数据表
        protected $table = null;
        // 自定义日志标题
        protected static $title = '';
        // 自定义日志内容
        protected static $content = '';

        /**
         * @throws \think\db\exception\BindParamException
         * @throws \think\exception\PDOException
         * @Author: LuckyHhy <jackhhy520@qq.com>
         * @name: initialize
         * @describe:
         */
        public function initialize()
        {
            parent::initialize();
            // 设置表名
            $this->table = DB_PREFIX.'log_'.date('Y').'_'.date('m');
            // 初始化行为日志表
            $this->initTable();
        }

        /**
         * 初始化行为日志表
         * @return string
         * @throws \think\db\exception\BindParamException
         * @throws \think\exception\PDOException
         * @author 牧羊人
         * @date 2019/4/4
         */
        public function initTable()
        {
            $tbl = $this->table;

            if (!$this->tableExists($this->table)) {
                $sql = "CREATE TABLE `{$tbl}` (
                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一性标识',
                  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行为ID',
                  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否后台操作：1是 2否',
                  `username` varchar(60) CHARACTER SET utf8mb4 NOT NULL COMMENT '操作人用户名',
                  `method` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '请求类型',
                  `module` varchar(30) NOT NULL COMMENT '模型',
                  `action` varchar(255) NOT NULL COMMENT '操作方法',
                  `url` varchar(200) CHARACTER SET utf8mb4 NOT NULL COMMENT '操作页面',
                  `param` text CHARACTER SET utf8mb4 NOT NULL COMMENT '请求参数(系统加密方式)',
                  `title` varchar(100) NOT NULL COMMENT '日志标题',
                  `content` varchar(1000) NOT NULL DEFAULT '' COMMENT '内容',
                  `ip` varchar(18) CHARACTER SET utf8mb4 NOT NULL COMMENT 'IP地址',
                  `user_agent` varchar(360) CHARACTER SET utf8mb4 NOT NULL COMMENT 'User-Agent',
                  `create_user` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加人',
                  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
                  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '有效标识：1正常 0删除',
                  PRIMARY KEY (`id`) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='系统行为日志表';";
                $this->query($sql);
            }

            return $tbl;
        }


        /**
         * 设置标题
         * @param [type] $title
         * @return void
         * @author Jackhhy
         * @date 2019-11-14
         */
        public function setTitle($title)
        {

            self::$title = $title;
        }

        /**
         * 设置内容
         * @param [type] $content
         * @return void
         * @author Jackhhy
         * @date 2019-11-14
         */
        public function setContent($content)
        {
            self::$content = $content;
        }

        /**
         * 记录行为日志
         * @return void
         * @author Jackhhy
         * @date 2019-11-14
         */
        public  function record()
        {
            // 日志数据
            $data = [
                'username'    => session("admin_name"),
                'module'      => request()->module(),
                'action'      => request()->url(),
                'method'      => request()->method(),
                'url'         => request()->url(true), // 获取完成URL
                'param'       => request()->param() ? Util::think_encrypt(serialize(request()->param())) : '',
                'title'       => self::$title,
                'content'     => self::$content,
                'ip'          => request()->ip(),
                'user_agent'  => request()->server('HTTP_USER_AGENT'),
                'create_user' => session("admin_id"),
                'create_time' => time(),
            ];
            // 日志入库
            self::insert($data);
        }


        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/10
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }



        /**
         * @param $id
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/25
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            // TODO: Implement del() method.
           return parent::JrkDelete($id);
        }


        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/14
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['userid']) && $param['userid'] !=0) {
                    $where['create_user'] =['create_user','=',$param['userid']];
                }
            }

            try{
                $data = Db::table($this->table)->where($where)->order($order)->page(PAGE)->limit(LIMIT)->select();
                $count =Db::table($this->table)->where($where)->count("id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }

        }


    }