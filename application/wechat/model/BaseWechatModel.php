<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/19-10:16
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\wechat\model;

    use think\Model;
    use traits\controller\Jump;

    /**
     * Class BaseWechatModel
     * @package app\wechat\model
     */
    class BaseWechatModel extends Model
    {


        use Jump;

        protected $update = ["update_time"];
        protected $insert = ["create_time", "ip"];


        /**
         * BaseWechatModel constructor.
         * @param array $data
         */
        public function __construct(array $data = [])
        {
            parent::__construct($data);

        }


        /**
         * @return int
         * @Author: LuckyHhy <jackhhy520@qq.com>
         * @name: setUpdateTimeAttr
         * @describe:
         */
        protected function setUpdateTimeAttr()
        {
            return time();
        }

        /**
         * @return mixed
         * @author: hhygyl
         * @name: setIpAttr
         * @describe:
         */
        protected function setIpAttr()
        {
            return request()->ip();
        }


        /**
         * @return int
         * @author: hhygyl
         * @name: setCreateTimeAttr
         * @describe:
         */
        public function setCreateTimeAttr()
        {
            return time();
        }

        /**
         * @param array $where
         * @param int $pageSize
         * @param array $order
         * @return array|\PDOStatement|string|\think\Collection
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: getList
         * @describe:
         */
        public static function getList($where = [], $pageSize = 10, $order = ['sort', 'id' => 'desc'])
        {
            return self::where($where)->where($order)->select();

        }

        /**
         * @param $id
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: getOne
         * @describe:
         */
        public static function getOne($id)
        {

            return self::find($id);
        }

        //表前缀

        /**
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: get_table_prefix
         * @describe:
         */
        public static function get_table_prefix()
        {

            return config("database.prefix");


        }

        /**
         * @param $data
         * @return bool|string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: add
         * @describe:
         */
        public function add($data)
        {
            $result = $this->save($data);
            if ($result) {
                return $result;
            } else {
                return '';
            }
        }



        /**
         * @param $data
         * @return string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: edit
         * @describe:
         */
        public function edit($data)
        {
            $info = $this->find($data['id']);
            unset($data['id']);
            $result = $info->save($data);
            if ($result) {
                return $result;
            } else {
                return '';
            }
        }

        /**
         * @param $ids
         * @return string
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: del
         * @describe:
         */
        public function del($ids)
        {
            if (is_array($ids)) {

                $info = $this->where('id', 'in', $ids)->select();
            } else {
                $info = $this->find($ids);
            }
            $result = $info->delete();
            if ($result) {
                return $result;
            } else {
                return '';
            }
        }

        /**
         * @param $data
         * @return string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19
         * @name: state
         * @describe:
         */
        public function state($data)
        {
            $id    = $data['id'];
            $field = $data['field'];
            if (empty($id)) {
                return '';
            }
            $info         = $this->find($id);
            $info->$field = $info->$field == 1 ? 0 : 1;
            $result       = $info->save();
            if ($result) {
                return $result;
            } else {
                return '';
            }

        }


    }