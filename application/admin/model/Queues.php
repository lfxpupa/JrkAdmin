<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-15:59
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use Cron\CronExpression;
    use think\db\Where;
    use think\Exception;

    /**
     * Class Queues
     * @package app\admin\model
     */
    class Queues extends BaseModel implements Comm
    {

        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25-16:10
         */
        protected $name = "sys_queue";


        /**
         * @var array
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25-16:10
         * 追加属性
         */
        protected $append = [
            'type_text'
        ];

        /**
         * @return array
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getTypeList
         * @describe:
         */
        public static function getTypeList()
        {
            return [
                'url'   => "请求URL",
                'sql'   => "执行SQL",
                'shell' => "执行Shell",
            ];
        }

        /**
         * @param $value
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/26
         * @name: getStatus
         * @describe:
         */
        public function getStatusAttr($value){
            $status = ['completed'=>'已完成','expired'=>'已过期',"hidden"=>'禁用',"normal"=>'正常'];
            return $status[$value];
        }

        /**
         * @param $value
         * @param $data
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getTypeTextAttr
         * @describe:
         */
        public function getTypeTextAttr($value, $data)
        {
            $typelist = self::getTypeList();
            $value = $value ? $value : $data['type'];
            return $value && isset($typelist[$value]) ? $typelist[$value] : $value;
        }

        /**
         * @param $value
         * @return false|int|string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: setBegintimeAttr
         * @describe:
         */
        protected function setBegintimeAttr($value)
        {
            return $value && !is_numeric($value) ? strtotime($value) : $value;
        }

        /**
         * @param $value
         * @return false|int|string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: setEndtimeAttr
         * @describe:
         */
        protected function setEndtimeAttr($value)
        {
            return $value && !is_numeric($value) ? strtotime($value) : $value;
        }

        /**
         * @param $value
         * @return false|int|string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: setExecutetimeAttr
         * @describe:
         */
        protected function setExecutetimeAttr($value)
        {
            return $value && !is_numeric($value) ? strtotime($value) : $value;
        }


        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['title']) && $param['title'] !='') {
                    $where['title'] =['like', "%".$param['title']."%"];
                }
                if(isset($param['type']) && $param['type'] !='') {
                    $where['type'] =$param['type'];
                }
            }
            try{
                $data = self::where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
                $count =self::where(new Where($where))->count("id");

                if (!empty($data)){
                    $time = time();
                    foreach ($data as $k =>$v) {
                        $cron = CronExpression::factory($v['schedule']);
                        $data[$k]['nexttime'] = $time > $v['endtime'] ? "无" : $cron->getNextRunDate()->getTimestamp();
                    }
                }
                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }
        }

        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }

        /**
         * @param $id
         * @return bool|mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: del
         * @describe:
         */
        public function del($id)
        {
            // TODO: Implement del() method.
            return parent::JrkDelete($id);
        }



        /**
         * @param $id
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/26
         * @name: getOne
         * @describe:
         */
        public function getOne($id){
            return self::find($id);
        }


    }