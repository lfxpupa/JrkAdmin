<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/5-15:07
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class Picture extends BaseValidate
    {

        protected $rule = [
            'category_id' =>  'checkId',
            'title'  =>  'require|max:100|token',
            'keywords' =>  'max:100',
            'description' =>  'max:200',
            'pwd'          => 'max:6',

        ];

        protected $message = [
            'title.require'  =>  '标题为必填',
            'title.max'  =>  '标题最多100个字符',
            'keywords.max'  =>  '关键词最多100个字符',
            'description.max' =>  '栏目描述最多200个字符',
            'pwd.max' =>  '密码最多6位',

        ];

        protected function checkId($value,$rule,$data){
            if($value < 1){
                return '栏目ID错误';
            }
            return true;
        }


    }