<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-11:49
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class DepartmentValidate extends BaseValidate
    {


        protected $rule = [
            'dname|token'  =>  'chsAlpha|max:20',
        ];

        protected $message = [
            'dname.chsAlpha'  =>  '部门名称只允许汉字、字母',
            'dname.max'  =>  '部门名称最大长度为20个字符',
        ];




    }