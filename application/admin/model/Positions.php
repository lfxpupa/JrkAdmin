<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-15:22
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;

    /**
     * Class Positions
     * @package app\admin\model
     */
    class Positions extends BaseModel
    {

        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25-15:23
         */
        protected $name = "co_positions";


        /**
         * @param array $param
         * @param string $order
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['pname']) && $param['pname'] !='') {
                    $where['pname'] =['like', "%".$param['pname']."%"];
                }
            }
            try{
                $data = self::where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
                $count =self::where(new Where($where))->count("id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }

        }



        /**
         * @param $data
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }

        /**
         * @param $id
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: del
         * @describe:
         */
        public function del($id)
        {
            // TODO: Implement del() method.
            return parent::JrkDelete($id);
        }



        /**
         * @param $id
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getOne
         * @describe:
         */
        public function getOne($id){
            return self::find($id);
        }


    }