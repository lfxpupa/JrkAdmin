<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2019/11/14 10:49
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------
    namespace app\common\behavior;
    use think\Db;
    use think\facade\Cache;
    use think\facade\Hook;
class InitHook
{
    /**
     * 执行行为 run方法是Behavior唯一的接口
     * @return void
     */
    public function run()
    {
        $data = Cache::get('Hooks');
        if (empty($data)) {
            //所有模块和插件钩子
            $hooks = Db::name('Hooks')->where('status', 1)->column('name,addons,modules');
            foreach ($hooks as $key => $value) {
                $hooks_class = [];
                //插件
                if ($value['addons']) {
                    $data = Db::name('Addons')->whereIn('name', $value['addons'])->where('status', 1)->column('name');
                    if ($data) {
                        $hooks_class = array_merge($hooks_class, array_map('get_addon_class', $data));
                    }
                }
                Hook::add($value['name'], $hooks_class);
            }
            Cache::set('Hooks', Hook::get());
        } else {
            //批量导入插件
            Hook::import($data, false);
        }

        // TODO...
    }
}