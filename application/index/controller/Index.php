<?php
namespace app\index\controller;


use Jrk\Email;
use Jrk\Util;
use think\Controller;
use think\facade\Env;


class Index extends Controller
{
    public function index()
    {
        return redirect(url('/admin/index'));
    }

}
