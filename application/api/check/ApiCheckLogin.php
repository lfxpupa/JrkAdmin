<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/12-11:47
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\api\check;


    /**
     * Class ApiCheckLogin
     * @package app\common\controller
     */
    class ApiCheckLogin extends ApiCheck
    {
        /**
         * @var
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/12-11:50
         */
        protected $user_info;

        /**
         * @return \think\response\Json|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/14 0014
         * @name: initialize
         * @describe:
         */
        public function initialize() {
            $header=$this->request->header();
            //获取请求token
            $token = isset($header['token'])?$header['token']:null;
            /**
             * 验证是否登录
             */
            if (is_null($token)) {
                header('Content-Type:application/json; charset=utf-8');
                exit(json_encode([
                    'errno' => ERRNO['SESSIONERR'],
                    'msg' => ERRNO_MAP['SESSIONERR']]));
            }

            /**
             * 验证登录是否过期
             */
            $user_info = getJWT($token);
            if (is_null($user_info)) {
                header('Content-Type:application/json; charset=utf-8');
                exit(json_encode([
                    'errno' => ERRNO['SESSIONERR'],
                    'msg' => '登录已过期']));
            }
            /**
             * 存储用户信息
             */
            $this->user_info = $user_info;
        }

    }