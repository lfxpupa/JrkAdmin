<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/2-9:36
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\controller;

    use think\Request;

    class Error
    {

        public function index(Request $request)
        {
            $cont = $request->controller();
            $meth=$request->method();
            if (!class_exists($cont)|| !method_exists($meth)){
                return view("public/404",['msg'=>'当前控制器：'.$cont."，方法：".$meth." 不存在"]);
            }

        }

    }