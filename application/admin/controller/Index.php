<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 14:52
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------

    namespace app\admin\controller;

    use app\admin\service\RoleService;

    use Jrk\Util;
    use think\Db;
    use think\facade\Cookie;

    class Index extends AdminBase
    {

        public function initialize()
        {
            parent::initialize();
        }

        /**
         * @return mixed|\think\response\View
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/13
         * @name: index
         * @describe:
         */
        public function index()
        {
            if (Cookie::has($this->admin_id.$this->admin_info['username'])) { //有锁屏进入锁屏页面
                $this->redirect(url("index/lock"));
                exit;
            }
            //dump(Cookie::get("admin_user"));die;
            $role = new RoleService();
            $info = $role->getOne(['id' => $this->admin_info['role_id']]);

            if ($info['status'] == 2) {
                $this->assign("msg", "您所在的权限组被禁用，请联系管理员解除！！");

                return $this->fetch('public/403');
            }

            return view();
        }




        public function home()
        {
            $data=db("recod")->order("id desc")->limit(8)->select();
            $this->assign("data",$data);
            //统计
            $this->people();

            return view();
        }


        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18 0018
         * @name: clearCache
         * @describe:
         */
        public function clearCache(){
            $this->cache("temp");
            $this->cache("cache");
            $this->cache("log");
            $this->success("缓存清除成功",'','',1);
        }

        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/28
         * @name: people
         * @describe:
         */
        public function people(){
            $yer=date("Y",time());
            $sql="SELECT MONTH( FROM_UNIXTIME( create_time ) )  'month', COUNT( * )  'number' FROM hhy_recod WHERE YEAR( FROM_UNIXTIME( create_time ) )=".$yer."
        GROUP BY MONTH( FROM_UNIXTIME( create_time ) )";
            $res=Db::query($sql);
            $houdata=array();

            if ($res){
                for ($i=0;$i<12;$i++){
                    $houdata[$i]=0;
                    foreach ($res as $k=>$t){
                        if($t['month']==$i+1){
                            $houdata[$i]=(int)$t['number'];
                        }
                    }
                }
                $houdata=($houdata);
            }else{
                for ($i=0;$i<12;$i++){
                    $houdata[$i]=0;
                }
                $houdata=($houdata);
            }
            $this->assign("history_tongji",$houdata);
        }


        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/24
         * @name: lockPass
         * @describe:设置锁屏密码
         */
        public function lockPass()
        {
            if ($this->request->isPost()) {
                $Pass = $this->request->post("pass");
                if (empty($Pass)) {
                    $this->error("锁屏密码不能为空");
                }
                Cookie::set($this->admin_id.$this->admin_info['username'], $Pass);//保存锁屏密码
                $this->success("锁屏成功");
            }
        }


        /**
         * @return mixed
         * @author: hhygyl <hhygyl520@qq.com>
         * @name: lock
         * @describe:锁屏页面
         */
        public function lock()
        {
            if (!Cookie::has($this->admin_id.$this->admin_info['username'])) { //解锁完毕
                $this->redirect(url("index/index"));
                exit;
            }

            return $this->fetch();
        }

        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/24
         * @name: unlock
         * @describe:解锁
         */
        public function unlock()
        {
            if ($this->request->isPost()) {
                $Pass = $this->request->post("pass");
                if (empty($Pass)) {
                    $this->error("锁屏密码不能为空");
                }

                $lockpass = Cookie::get($this->admin_id.$this->admin_info['username']);
                if ($lockpass == $Pass) {
                    Cookie::delete($this->admin_id.$this->admin_info['username']); //清除解锁密码
                    $this->success("解锁成功");
                } else {
                    $this->error("锁屏密码错误！");
                }
            }
        }



        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/24
         * @name: passLock
         * @describe:管理员密码解锁
         */
        public function passLock()
        {
            if ($this->request->isPost()) {
                $Pass = $this->request->post("password");
                if (empty($Pass)) {
                    $this->error("管理员登录密码不能为空");
                }

                if (Util::think_decrypt($this->admin_info['password']) == $Pass) {
                    Cookie::delete($this->admin_id.$this->admin_info['username']); //清除解锁密码
                    $this->success("解锁成功");
                } else {
                    $this->error("管理登录密码错误！");
                }
            }
        }



        /**
         * @return string
         * @throws \Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/2
         * @name: weather
         * @describe:天气
         */
        public function weather()
        {

            return $this->fetch();
        }



    }
