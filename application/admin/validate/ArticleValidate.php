<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/4-10:45
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class ArticleValidate extends BaseValidate
    {

        protected $rule = [
            'category_id' =>  'checkId',
            'title'  =>  'require|max:150|token',
            'keywords' =>  'max:100',
            'description' =>  'max:200',
        ];

        protected $message = [
            'title.require'  =>  '标题为必填',
            'title.max'  =>  '标题最多150个字符',
            'keywords.max'  =>  '关键词最多100个字符',
            'description.max' =>  '文章描述最多200个字符',

        ];


        protected function checkId($value,$rule,$data){
            if($value < 1){
                return '栏目分类ID错误';
            }
            return true;
        }

    }