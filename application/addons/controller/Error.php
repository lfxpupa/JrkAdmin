<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/18-10:09
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\addons\controller;

    use think\Request;

    class Error
    {

        /**
         * @param Request $request
         * @return mixed
         * @throws \think\Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: _empty
         * @describe:
         */
        public function _empty(Request $request)
        {
            $controller = \think\Loader::parseName($request->controller());
            $action     = $request->action();
            $class      = "Admin";

            if (!pluginActionExists($controller, $class, $action)) {

                throw new \think\Exception("插件{$controller}实例化错误！");

            }
            $object = \think\Container::get("\\addons\\".$controller."\\controller\\{$class}");

            return $object->$action();
        }

    }