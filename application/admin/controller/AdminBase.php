<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 14:44
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------
    namespace app\admin\controller;


    use app\admin\service\MenuService;
    use app\admin\service\RoleService;
    use app\common\controller\BaseController;

    use Jrk\Trees;

    class AdminBase extends BaseController
    {
        // 模型
        protected $model;
        // 服务
        protected $service;
        // 校验
        protected $validate;

        //删除日志
        protected $del = '';

        // 登录ID
        protected $admin_id;
        // 登录信息
        protected $admin_info;
        // 权限
        protected $system_auth;



        /**
         * @throws \think\Exception
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @throws \think\exception\PDOException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/14 0014
         * @name: initialize
         * @describe:
         */
        public function initialize()
        {

            parent::initialize();

            $this->admin_info = $this->app->session->get(config('app.user_info_session'));

            //用户信息
            $this->assign("admin_info", $this->admin_info);

            //初始化用户信息
            $this->AdminInfo();
            //菜单列表
            $this->menuList();
        }



        /**
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/3
         * @name: index
         * @describe: 获取分页数据
         */
        public function index()
        {
            //获取分页数据
            if (IS_AJAX) {
                return $this->model->getAdminPageData($this->param);
            }

            return $this->fetch();
        }


        /**
         * @return mixed
         * @throws \think\Exception
         * @throws \think\exception\PDOException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/12 0012
         * @name: del
         * @describe:删除
         */
        public function del()
        {
            if (IS_AJAX) {
                $ids = $this->request->post("ids");
                if (is_array($ids)) {
                    $i = implode(",", $ids);
                } else {
                    $i = $ids;
                }
                addLog("删除", "删除".$this->del."ID：".$i);

                return $this->model->del($ids);
            }
        }



        /**
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/3
         * @name: AdminInfo
         * @describe:
         */
        public function AdminInfo()
        {
            $this->admin_id    = session("admin_id");
            $this->system_auth = session("admin_role_id");

        }



        /**
         * @return mixed
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/13
         * @name: menuList
         * @describe: 左侧菜单列表
         */
        public function menuList()
        {
            $role_id = $this->app->session->get("admin_role_id");
            $role    = new RoleService();
            if ($role_id == 1) { //超级管理员
                $data = MenuService::menuList();
                $data = Trees::DeepTree($data);
            } else {
                $info = $role->getOne(['id' => $role_id]);
                //当前权限组被禁用
                if ($info['status'] == 2) {
                    $this->assign("menulist", []);

                    return;
                }
                $ids  = unserialize($info['rule']);
                $data = MenuService::menuList(['id' => $ids]);
                $data = Trees::DeepTree($data);
            }
            //dump($data);
            $this->assign("menulist", $data);
        }



    }
