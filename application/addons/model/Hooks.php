<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2020/3/21 0021-14:52
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------

namespace app\addons\model;


use app\admin\imple\Comm;
use app\common\model\BaseModel;
use think\db\Where;
use think\Exception;

/**
 * Class Hooks
 * @package app\addons\model
 */
class Hooks extends BaseModel implements Comm
{

    /**
     * @var string
     */
    protected $table =DB_PREFIX."hooks";


    /**
     * @param array $param
     * @param string $order
     * @return mixed|void
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/21 0021
     * @name: getAdminPageData
     * @describe:
     */
    public function getAdminPageData($param = [], $order = 'id desc')
    {
        // TODO: Implement getAdminPageData() method.
        $where = [];
        if(!empty($param)) {
            //搜索条件
            if(isset($param['name']) && $param['name'] !='') {
                $where['name'] =['like', "%".$param['name']."%"];
            }
        }
        try{
            $data = self::where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
            $count =self::where(new Where($where))->count("id");

            $this->ajaxResult($data,0,'',$count);

        }catch (Exception $exception){

            $this->ajaxResult('',100,$exception->getMessage());
        }
    }


    /**
     * @param $data
     * @return mixed|void
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/21 0021
     * @name: addAndEdit
     * @describe:
     */
    public function addAndEdit($data)
    {
        // TODO: Implement addAndEdit() method.
        return parent::doAll($data);
    }

    /**
     * @param $id
     * @return bool|mixed
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/21 0021
     * @name: del
     * @describe:
     */
    public function del($id)
    {
        // TODO: Implement del() method.
        return parent::JrkDelete($id);
    }


    /**
     * @param $id
     * @param int $type
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/21 0021
     * @name: isOkOrFalse
     * @describe:操作
     */
    public function isOkOrFalse($id,$type=0){
        if (is_array($id)){
            $ids=$id;
        }else{
            $ids=@explode(",",$id);
        }
        $result = self::where('id', 'in',$ids)->setField("status",$type);
        if ($result) {
            $this->success("操作成功");
        } else {
            $this->error("操作失败");
        }
    }



}