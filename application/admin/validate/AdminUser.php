<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/2/26-16:05
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;

    use app\common\validate\BaseValidate;

    class AdminUser extends BaseValidate
    {

        protected $rule = [
            'nickname|token'  =>  'chsDash|max:12',
            'username' =>  'alphaNum|max:12',
            'password' =>  'min:6|max:16|alphaDash',
            'repassword' =>  'confirm:password',
        ];

        protected $message = [
            'nickname.chsAlpha'  =>  '管理员昵称只允许汉字、字母',
            'nickname.max'  =>  '用户昵称最大长度为12个字符',
            'username.alphaNum'  =>  '用户名只允许字母与数字',
            'username.max'  =>  '用户名最大长度为12个字符',
            'password.min'  =>  '密码最少6个字符',
            'password.max' =>  '密码最长16个字符',
            'password.alphaNum' =>  '密码只允许字母、数字',
        ];


        /**
         * 验证场景
         */
        protected $scene = [
            'edit'  =>  ['username','nickname'],
        ];

    }