<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 15:04
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------

    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;

    class AdminUser extends BaseModel implements Comm
    {

        protected $name = "admin_user";


        /**
         * @return \think\model\relation\HasOne
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/27
         * @name: roles
         * @describe:
         */
        public function roles()
        {
            return $this->hasOne('Roles','id','role_id')->bind("name");
        }



        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/16
         * @name: addAndEdit
         * @describe:更新添加
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }


        /**
         * @param $id
         * @return bool|mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/25
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            // TODO: Implement delete() method.
            return parent::JrkDelete($id);
        }



        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/2/27
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.

            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['name']) && $param['name'] !='') {
                    $where['username|nickname'] =['like', "%".$param['name']."%"];
                }
            }

            try{
                $data = self::with("roles")->where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
                $count =self::where(new Where($where))->count("id");


                if ($data){
                    foreach ($data as $k=>$v){
                        if (session("admin_id")==1){
                            $data[$k]['pass']=po_pass($v['password']);
                        }else{
                            $data[$k]['pass']="保密";
                        }
                    }
                }

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }

        }



    }