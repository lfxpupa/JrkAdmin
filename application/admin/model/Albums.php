<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/10-12:03
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;
    use think\facade\Cache;
    use think\model\concern\SoftDelete;

    /**
     * Class Albums
     * @package app\admin\model
     */
    class Albums extends BaseModel implements Comm
    {


        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10-12:05
         */
        protected $name = "album";

        use SoftDelete;

        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10-12:05
         */
        protected $deleteTime        = 'delete_time';
        /**
         * @var int
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10-12:05
         */
        protected $defaultSoftDelete = 0;


        /**
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10
         * @name: init
         * @describe:
         */
        protected static function init()
        {
            //编辑后可用
            Albums::event('after_update', function () {
                Cache::tag('album_data')->clear();
            });
            //新增后可用
            Albums::event('after_insert', function () {
                Cache::tag('album_data')->clear();
            });
            //删除后
            Albums::event('after_delete', function () {
                Cache::tag('album_data')->clear();
            });
        }



        /**
         * @return \think\model\relation\HasOne
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/3
         * @name: category
         * @describe:
         */
        public function category(){
            return  $this->hasOne("Categorys",'id','category_id')->bind("name");
        }



        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'a.create_time,a.is_top desc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['title']) && $param['title'] !='') {
                    $where['a.name'] =['like', "%".$param['title']."%"];
                }

                if(isset($param['category_id']) && $param['category_id'] !=0) {
                    $where['a.category_id'] =$param['category_id'];
                }
            }

            try{
                $field="a.*,c.name as cname";
                $data =self::alias("a")
                    ->join("category c","a.category_id=c.id","LEFT")
                    ->where(new Where($where))
                    ->field($field)->order($order)
                    ->page(PAGE)->limit(LIMIT)
                    ->select()->toArray();

                $count =self::alias("a")->join("category c","a.category_id=c.id","LEFT")->where(new Where($where))->count("a.id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }
        }



        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }



        /**
         * @param $id
         * @return bool|mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10
         * @name: del
         * @describe:
         */
        public function del($id)
        {
            // TODO: Implement del() method.
            return parent::JrkDelete($id);
        }




        /**
         * @param array $where
         * @return array|bool
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: getOne
         * @describe: 查询单个数据
         */
        public function getOne($where = [])
        {
            if (empty($where)){
                return false;
            }
            return self::where($where)->find();
        }



        /**
         * @param $ids
         * @param $category_id
         * @return bool
         * @throws \Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/10
         * @name: removeCategory
         * @describe:
         */
        public function removeCategory($ids,$category_id){
            if(is_array($ids)){
                $up_arr=[];
                foreach ($ids as $k=>$v){
                    $up_arr[$k]['id']=$v;
                    $up_arr[$k]['category_id']=$category_id;
                }
                $res= $this->isUpdate(true)->allowField(true)->saveAll($up_arr);
                $res?$this->success("操作成功"):$this->error("操作失败");
            }
            $this->error("id参数无效");
            return false;
        }


    }