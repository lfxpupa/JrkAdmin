<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-14:44
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use think\db\Where;
    use think\Exception;

    /**
     * Class Stations
     * @package app\admin\model
     */
    class Stations extends BaseModel implements Comm
    {

        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25-15:23
         */
        protected $name = "co_station";



        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['sname']) && $param['sname'] !='') {
                    $where['sname'] =['like', "%".$param['sname']."%"];
                }
            }
            try{
                $data = self::with("depart")->where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
                $count =self::with("depart")->where(new Where($where))->count("id");

                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }

        }



        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
            return parent::doAll($data);
        }

        /**
         * @param $id
         * @return bool|mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: del
         * @describe:
         */
        public function del($id)
        {
            // TODO: Implement del() method.
            return parent::JrkDelete($id);
        }



        /**
         * @param $id
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getOne
         * @describe:
         */
        public function getOne($id){
            return self::find($id);
        }

        /**
         * @return \think\model\relation\HasOne
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: depart
         * @describe:
         */
        public function depart(){
            return $this->hasOne("Departments","id","d_id")->bind("dname");
        }


    }