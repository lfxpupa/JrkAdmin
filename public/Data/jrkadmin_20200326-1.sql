/*
Navicat MySQL Data Transfer

Source Server         : 本地连接
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : jrkadmin

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-03-26 12:45:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hhy_addons
-- ----------------------------
DROP TABLE IF EXISTS `hhy_addons`;
CREATE TABLE `hhy_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `update_time` int(11) DEFAULT '0',
  `ip` varchar(55) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='插件表';

-- ----------------------------
-- Records of hhy_addons
-- ----------------------------
INSERT INTO `hhy_addons` VALUES ('5', 'saiyousms', '短信插件', '短信插件-by赛邮', '{\"appid\":\"\",\"appkey\":\"\"}', '御宅男', '1.0.0', '1584534729', '0', '0', '0', '127.0.0.1');

-- ----------------------------
-- Table structure for hhy_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `hhy_admin_user`;
CREATE TABLE `hhy_admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `nickname` varchar(100) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `role_id` int(11) unsigned NOT NULL COMMENT '角色ID',
  `logins` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `create_time` int(11) unsigned NOT NULL,
  `ip` varchar(90) NOT NULL COMMENT '登录IP',
  `login_at` int(11) NOT NULL DEFAULT '0' COMMENT '登录时间',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1正常 2禁用',
  `tokens` varchar(255) DEFAULT NULL COMMENT 'token',
  `cookie` varchar(255) DEFAULT NULL COMMENT '记录登陆状态',
  `sex` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role-id` (`role_id`) USING BTREE,
  KEY `s` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='后台用户表';

-- ----------------------------
-- Records of hhy_admin_user
-- ----------------------------
INSERT INTO `hhy_admin_user` VALUES ('1', 'jrkadmin', 'MDAwMDAwMDAwMJOnr6KRi4PXwoeVcg', 'jrkAdmin', '/uploads/images/20200228/cde0b8d4fd528515da36b0ab067ebcbf.jpg', '1', '23', '1585099790', '127.0.0.1', '1585099790', '1', '32f3e5ee38464e97cbeeb353432f796b4250c38e', 'fa5edf0c6c023297851e436a4318ac3db614d3c7', '女');

-- ----------------------------
-- Table structure for hhy_album
-- ----------------------------
DROP TABLE IF EXISTS `hhy_album`;
CREATE TABLE `hhy_album` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL COMMENT '栏目ID',
  `name` varchar(255) NOT NULL COMMENT '相册名字',
  `describe` varchar(255) DEFAULT NULL COMMENT '相册描述',
  `tags` varchar(255) DEFAULT NULL COMMENT '标签',
  `nums` int(11) NOT NULL DEFAULT '0' COMMENT '相册图片数量',
  `img` varchar(255) DEFAULT NULL COMMENT '封面图',
  `create_time` int(11) unsigned NOT NULL,
  `password` varchar(255) DEFAULT NULL COMMENT '相册密码',
  `is_open` tinyint(2) NOT NULL DEFAULT '0' COMMENT '1需要密码 0 不需要',
  `is_show` tinyint(2) NOT NULL DEFAULT '0' COMMENT '1显示 、0不显示',
  `update_time` int(11) unsigned DEFAULT '0',
  `delete_time` int(11) unsigned DEFAULT '0',
  `ip` varchar(25) DEFAULT NULL,
  `is_top` tinyint(2) NOT NULL DEFAULT '0' COMMENT '1置顶 0不指定',
  `hits` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '点击数量',
  `images` text COMMENT '相册图片',
  PRIMARY KEY (`id`),
  KEY `is_show` (`is_show`) USING BTREE,
  KEY `is` (`category_id`) USING BTREE,
  KEY `top` (`is_top`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='相册表';

-- ----------------------------
-- Records of hhy_album
-- ----------------------------
INSERT INTO `hhy_album` VALUES ('1', '4', '测试测试', '暂无', '打算,阿萨德,爱迪生', '5', '/uploads/album/20200310/78d952232097113577298b368a34b79b.jpg', '1583825551', '', '0', '1', '1583831429', '0', '127.0.0.1', '0', '100', 'a:5:{i:0;s:61:\"/uploads/album/20200310/9aebc570e201a0d5c9977bfba548d493.jpeg\";i:1;s:61:\"/uploads/album/20200310/fd7b2ae3a2a4556446de5031344274d1.jpeg\";i:2;s:60:\"/uploads/album/20200310/ceea0bb39fe67d8009c842a99ee01b6e.png\";i:3;s:60:\"/uploads/album/20200310/78d952232097113577298b368a34b79b.jpg\";i:4;s:60:\"/uploads/album/20200310/f8ce00f356e92ab92b31f07264a4b3f7.jpg\";}');

-- ----------------------------
-- Table structure for hhy_article
-- ----------------------------
DROP TABLE IF EXISTS `hhy_article`;
CREATE TABLE `hhy_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL COMMENT '栏目ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `keywords` varchar(100) DEFAULT '' COMMENT '标签',
  `description` varchar(200) DEFAULT '' COMMENT '摘要',
  `image_url` varchar(200) DEFAULT '' COMMENT '图片',
  `content` longtext COMMENT '内容',
  `author` varchar(20) DEFAULT '' COMMENT '文章作者',
  `source` varchar(30) DEFAULT '' COMMENT '文章来源',
  `hits` int(10) unsigned DEFAULT '0' COMMENT '点击量',
  `comment_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论数量',
  `is_recommend` tinyint(1) DEFAULT '0' COMMENT '是否推荐',
  `is_top` tinyint(4) DEFAULT '0' COMMENT '是否置顶',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示  0为不显示 1为显示',
  `url` varchar(250) DEFAULT '' COMMENT '文章链接',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `delete_time` int(11) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  KEY `hits` (`hits`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文章表';

-- ----------------------------
-- Records of hhy_article
-- ----------------------------
INSERT INTO `hhy_article` VALUES ('1', '4', '测试文章', '阿打算打', '大萨达撒', '', '大撒打算多热无无热无若翁认为', '作者', '来源', '5', '9', '0', '1', '1', '/jjsss', '1583205968', '0', '0', '127.0.0.1');

-- ----------------------------
-- Table structure for hhy_category
-- ----------------------------
DROP TABLE IF EXISTS `hhy_category`;
CREATE TABLE `hhy_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级id  顶级为0',
  `model_id` int(11) NOT NULL DEFAULT '0' COMMENT '模型id  对应model表',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '栏目描述',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否导航栏显示  1显示0不显示',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '栏目链接',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '排序 asc 升序',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `index_template` varchar(50) NOT NULL DEFAULT 'index' COMMENT '主页模版',
  `list_template` varchar(50) NOT NULL DEFAULT 'list' COMMENT '列表模板',
  `show_template` varchar(50) NOT NULL DEFAULT 'show' COMMENT '详情页模板',
  `ip` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `is_show` (`is_show`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文章分类表';

-- ----------------------------
-- Records of hhy_category
-- ----------------------------
INSERT INTO `hhy_category` VALUES ('3', '0', '2', '技术学习', '啊啊', '1', '/a-i/3.html', '10', '1583305903', '1583305903', 'index', 'lists', 'show', '127.0.0.1');
INSERT INTO `hhy_category` VALUES ('4', '3', '2', 'java', '', '1', '/a-l/4.html', '10', '1583305919', '1583305919', 'index', 'lists', 'show', '127.0.0.1');
INSERT INTO `hhy_category` VALUES ('5', '3', '2', 'php', '', '1', '/a-l/5.html', '10', '1583305929', '1583305929', 'index', 'lists', 'show', '127.0.0.1');

-- ----------------------------
-- Table structure for hhy_comment
-- ----------------------------
DROP TABLE IF EXISTS `hhy_comment`;
CREATE TABLE `hhy_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a_id` int(11) NOT NULL DEFAULT '0' COMMENT '内容id',
  `c_id` int(11) NOT NULL COMMENT '分类id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级id  如果不等于0  表示楼中楼',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '评论内容',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '发送者信息 后期增加用户登录时可变成用户id',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT 'ip',
  `reply` int(11) NOT NULL DEFAULT '0' COMMENT '回复数量',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `is_show` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 不显示 1显示',
  `is_ok` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0未审核 1已审核',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `a_id` (`a_id`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='文章用户评论表';

-- ----------------------------
-- Records of hhy_comment
-- ----------------------------
INSERT INTO `hhy_comment` VALUES ('1', '1', '2', '0', '测试测试', '1', '127.0.0.1', '0', '1583205968', '0', '1', '0');
INSERT INTO `hhy_comment` VALUES ('2', '1', '2', '1', '大萨达撒', '2', '127.0.01', '0', '1583205968', '0', '0', '0');
INSERT INTO `hhy_comment` VALUES ('3', '1', '2', '1', '大萨达', '9', '127.0.01', '0', '1583205968', '0', '0', '0');
INSERT INTO `hhy_comment` VALUES ('4', '1', '2', '2', '撒打算www', '7', '127.0.0.1', '0', '1583205968', '0', '0', '0');

-- ----------------------------
-- Table structure for hhy_co_department
-- ----------------------------
DROP TABLE IF EXISTS `hhy_co_department`;
CREATE TABLE `hhy_co_department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dname` varchar(100) NOT NULL COMMENT '部门名称',
  `charger` varchar(100) DEFAULT '',
  `charger_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '负责人ID',
  `snum` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '部门下目前人数',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0关闭 1启用',
  `pnum` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '人数编制',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `beizhu` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  `ip` varchar(55) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';

-- ----------------------------
-- Records of hhy_co_department
-- ----------------------------
INSERT INTO `hhy_co_department` VALUES ('1', '策划部', 'ads', '0', '0', '1', '111', '0', 'dsaasda', '1585108604', '0', '127.0.0.1');
INSERT INTO `hhy_co_department` VALUES ('2', '营销推广组', 'dsada', '0', '0', '1', '222', '1', '32323', '1585116979', '0', '127.0.0.1');
INSERT INTO `hhy_co_department` VALUES ('3', 'IT部', '送达', '0', '0', '1', '22', '0', '爱仕达多所', '1585117989', '0', '127.0.0.1');
INSERT INTO `hhy_co_department` VALUES ('4', '人事部', '爱迪生', '0', '0', '1', '23', '0', '敖德萨所多', '1585118024', '0', '127.0.0.1');
INSERT INTO `hhy_co_department` VALUES ('5', '生信部', '大萨达', '0', '0', '1', '33', '0', '', '1585118117', '0', '127.0.0.1');
INSERT INTO `hhy_co_department` VALUES ('6', '程序组', '加', '0', '0', '1', '4', '3', '', '1585120583', '0', '127.0.0.1');
INSERT INTO `hhy_co_department` VALUES ('7', '后勤组', '搜索', '0', '0', '1', '3', '3', '', '1585120598', '0', '127.0.0.1');

-- ----------------------------
-- Table structure for hhy_co_positions
-- ----------------------------
DROP TABLE IF EXISTS `hhy_co_positions`;
CREATE TABLE `hhy_co_positions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pname` varchar(255) NOT NULL DEFAULT '' COMMENT '职级名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 关闭 1启用',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='职级表';

-- ----------------------------
-- Records of hhy_co_positions
-- ----------------------------
INSERT INTO `hhy_co_positions` VALUES ('1', '试用员工', '1', '1585121652', '0', '127.0.0.1');
INSERT INTO `hhy_co_positions` VALUES ('2', '正式员工', '1', '1585121776', '0', '127.0.0.1');
INSERT INTO `hhy_co_positions` VALUES ('3', '实习生', '1', '1585121787', '0', '127.0.0.1');
INSERT INTO `hhy_co_positions` VALUES ('4', '兼职生', '1', '1585121796', '0', '127.0.0.1');
INSERT INTO `hhy_co_positions` VALUES ('5', '主管级别', '1', '1585121828', '0', '127.0.0.1');
INSERT INTO `hhy_co_positions` VALUES ('6', '部门经理', '1', '1585121843', '0', '127.0.0.1');
INSERT INTO `hhy_co_positions` VALUES ('7', '总经理', '1', '1585121854', '0', '127.0.0.1');

-- ----------------------------
-- Table structure for hhy_co_station
-- ----------------------------
DROP TABLE IF EXISTS `hhy_co_station`;
CREATE TABLE `hhy_co_station` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sname` varchar(100) NOT NULL DEFAULT '' COMMENT '岗位名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1正常 0关闭',
  `d_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属部门id',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(25) DEFAULT NULL,
  `beizhu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`) USING BTREE,
  KEY `p_id` (`d_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='岗位表';

-- ----------------------------
-- Records of hhy_co_station
-- ----------------------------
INSERT INTO `hhy_co_station` VALUES ('1', 'PHP开发工程师', '1', '1', '1585120488', '0', '127.0.0.1', '打算');
INSERT INTO `hhy_co_station` VALUES ('2', 'python爬虫开发工程师', '1', '1', '1585120516', '0', '127.0.0.1', '');
INSERT INTO `hhy_co_station` VALUES ('3', 'php开发工程师', '1', '6', '1585120541', '1585120625', '127.0.0.1', '');
INSERT INTO `hhy_co_station` VALUES ('4', '后勤管理员', '1', '7', '1585120566', '1585120636', '127.0.0.1', '负责公司后勤');

-- ----------------------------
-- Table structure for hhy_friendlink
-- ----------------------------
DROP TABLE IF EXISTS `hhy_friendlink`;
CREATE TABLE `hhy_friendlink` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8 NOT NULL,
  `href` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  `ip` varchar(25) CHARACTER SET utf8 NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT '站长邮箱',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1 显示  0 不显示',
  `type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1 友情链接 2 站长推荐',
  `desc` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  KEY `ty` (`type`) USING BTREE,
  KEY `s` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='友情链接';

-- ----------------------------
-- Records of hhy_friendlink
-- ----------------------------
INSERT INTO `hhy_friendlink` VALUES ('2', 'lucky导航', 'http://dao.jackhhy.cn/', '1563334799', '1566271083', '127.0.0.1', '1', '2237696520@qq.com', '1', '2', '');
INSERT INTO `hhy_friendlink` VALUES ('3', 'LuckyAdmin', 'https://gitee.com/luckygyl/LuckyAdmin', '1563339986', '1566271040', '127.0.0.1', '1', 'jackhhy520@qq.com', '1', '2', '');
INSERT INTO `hhy_friendlink` VALUES ('4', '莫愁', 'https://www.mochoublog.com', '1566271204', '0', '127.0.0.1', '1', 'sss@qq.com', '1', '1', '');
INSERT INTO `hhy_friendlink` VALUES ('6', '智博客', 'https://www.nbclass.com/', '1566545996', null, '222.247.165.5', '1', '', '1', '1', null);
INSERT INTO `hhy_friendlink` VALUES ('7', '宝塔面板', 'https://www.bt.cn/', '1566547393', '1566547663', '222.247.165.5', '1', '', '1', '2', null);
INSERT INTO `hhy_friendlink` VALUES ('8', '悠悠吧', 'https://www.usuuu.com', '1566547484', null, '222.247.165.5', '1', '547241650@qq.com', '1', '1', null);
INSERT INTO `hhy_friendlink` VALUES ('9', 'LNMP一键安装包.', 'https://lnmp.org/', '1566547591', null, '222.247.165.5', '1', '', '1', '2', null);
INSERT INTO `hhy_friendlink` VALUES ('10', '恒哥博客', 'https://www.qq123.xin/', '1566547814', null, '222.247.165.5', '1', '304587661@qq.com', '1', '1', null);
INSERT INTO `hhy_friendlink` VALUES ('11', '脚本之家', 'https://www.jb51.net/', '1566547975', null, '222.247.165.5', '1', '', '1', '2', null);
INSERT INTO `hhy_friendlink` VALUES ('12', '博客大全', 'https://daohang.lusongsong.com/', '1566548287', null, '222.247.165.5', '1', '', '1', '1', null);
INSERT INTO `hhy_friendlink` VALUES ('13', 'WXiangQian', 'https://wxiangqian.github.io/', '1566625442', null, '119.39.71.248', '1', '', '1', '1', null);
INSERT INTO `hhy_friendlink` VALUES ('14', '影视全搜索', 'http://v.eyunzhu.com/', '1566782385', null, '222.247.166.78', '1', '', '1', '2', null);

-- ----------------------------
-- Table structure for hhy_hooks
-- ----------------------------
DROP TABLE IF EXISTS `hhy_hooks`;
CREATE TABLE `hhy_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  `modules` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的模块 ''，''分割',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否系统',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='钩子';

-- ----------------------------
-- Records of hhy_hooks
-- ----------------------------
INSERT INTO `hhy_hooks` VALUES ('1', 'pageHeader', '页面header钩子，一般用于加载插件CSS文件和代码', '1', '1509174020', '', '', '1', '1', '1509174020', '');
INSERT INTO `hhy_hooks` VALUES ('2', 'pageFooter', '页面footer钩子，一般用于加载插件JS文件和JS代码', '1', '1509174020', '', '', '1', '1', '1509174020', '');
INSERT INTO `hhy_hooks` VALUES ('3', 'smsSend', '短信发送', '2', '1509174020', 'saiyousms', '', '1', '1', '1509174020', '');
INSERT INTO `hhy_hooks` VALUES ('4', 'smsNotice', '短信通知', '2', '1509174020', 'saiyousms', '', '1', '1', '1509174020', '');

-- ----------------------------
-- Table structure for hhy_log_2020_03
-- ----------------------------
DROP TABLE IF EXISTS `hhy_log_2020_03`;
CREATE TABLE `hhy_log_2020_03` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一性标识',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行为ID',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否后台操作：1是 2否',
  `username` varchar(60) NOT NULL COMMENT '操作人用户名',
  `method` varchar(20) NOT NULL COMMENT '请求类型',
  `module` varchar(30) NOT NULL COMMENT '模型',
  `action` varchar(255) NOT NULL COMMENT '操作方法',
  `url` varchar(200) NOT NULL COMMENT '操作页面',
  `param` text NOT NULL COMMENT '请求参数(系统加密方式)',
  `title` varchar(100) NOT NULL COMMENT '日志标题',
  `content` varchar(1000) NOT NULL DEFAULT '' COMMENT '内容',
  `ip` varchar(18) NOT NULL COMMENT 'IP地址',
  `user_agent` varchar(360) NOT NULL COMMENT 'User-Agent',
  `create_user` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加人',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '有效标识：1正常 0删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='系统行为日志表';

-- ----------------------------
-- Records of hhy_log_2020_03
-- ----------------------------

-- ----------------------------
-- Table structure for hhy_menu
-- ----------------------------
DROP TABLE IF EXISTS `hhy_menu`;
CREATE TABLE `hhy_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(55) CHARACTER SET utf8 NOT NULL COMMENT '菜单名称',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1显示  2 不显示',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `spread` char(6) CHARACTER SET utf8 DEFAULT NULL,
  `fontFamily` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `is_menu` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1菜单  2按钮',
  `icon` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `target` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT '_blank 跳转出',
  `href` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '菜单地址',
  `create_time` int(11) NOT NULL,
  `ip` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `update_time` int(11) NOT NULL DEFAULT '0',
  `isClose` char(6) CHARACTER SET utf8 DEFAULT NULL,
  `isCheck` char(6) CHARACTER SET utf8 DEFAULT NULL,
  `listorder` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `is_menu` (`is_menu`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE,
  KEY `ss` (`listorder`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COMMENT='菜单按钮表';

-- ----------------------------
-- Records of hhy_menu
-- ----------------------------
INSERT INTO `hhy_menu` VALUES ('2', '数据库管理', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-template-1', '', '', '1578638603', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('3', '数据备份', '1', '2', 'false', 'layui-icon', '1', 'layui-icon-rate-half', '', '/admin/databack/index', '1578638769', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('4', '数据还原', '1', '2', 'false', 'layui-icon', '1', 'layui-icon-rate-half', '', '/admin/databack/importlist', '1578638782', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('6', '系统管理', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-set-fill', '', '', '1578640765', '127.0.0.1', '0', '', '', '800');
INSERT INTO `hhy_menu` VALUES ('7', '菜单管理', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-spread-left', '', '/admin/menu/index', '1578640809', '127.0.0.1', '0', '', '', '130');
INSERT INTO `hhy_menu` VALUES ('8', '角色权限', '1', '54', 'false', 'layui-icon', '1', 'layui-icon-senior', '', '/admin/role/index', '1578646752', '127.0.0.1', '1579145630', '', '', '1');
INSERT INTO `hhy_menu` VALUES ('9', '微信管理', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-login-wechat', '', '', '1578969539', '127.0.0.1', '0', '', '', '600');
INSERT INTO `hhy_menu` VALUES ('10', '微信账号', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-set', '', '/wechat/wechat/index', '1578969574', '127.0.0.1', '1578969907', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('11', '微信菜单', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-snowflake', '', '/wechat/wechat/menu', '1578969596', '127.0.0.1', '1578969921', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('12', '微信粉丝', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-rmb', '', '/wechat/wechat/fans', '1578969609', '127.0.0.1', '1578969937', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('13', '粉丝标签', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-login-wechat', '', '/wechat/wechat/tag', '1578969637', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('14', '历史消息', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-template', '', '/wechat/wechat/message', '1578969659', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('15', '微信素材', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-user', '', '/wechat/wechat/material', '1578969693', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('16', '微信二维码', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-tips', '', '/wechat/wechat/qrcode', '1578969791', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('17', '微信回复', '1', '9', 'false', 'layui-icon', '1', 'layui-icon-unlink', '', '/wechat/wechat/reply', '1578969815', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('20', '管理人员', '1', '54', 'false', 'layui-icon', '1', 'layui-icon-username', '', '/admin/admin/index', '1578970072', '127.0.0.1', '1579145645', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('21', '系统配置', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-set', '', '/admin/setting/system', '1578970273', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('22', '日志管理', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-list', '', '', '1578970315', '127.0.0.1', '1585037071', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('23', '系统消息', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-notice', '', '', '1578970332', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('25', '存储配置', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-template-1', '', '/admin/setting/store', '1578970395', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('49', '组织机构', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-tree', '', '', '1579145075', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('50', '部门管理', '1', '49', 'false', 'layui-icon', '1', 'layui-icon-shrink-right', '', '/admin/company.department/index', '1579145257', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('51', '岗位管理', '1', '49', 'false', 'layui-icon', '1', 'layui-icon-more-vertical', '', '/admin/company.station/index', '1579145274', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('52', '职级管理', '1', '49', 'false', 'layui-icon', '1', 'layui-icon-template', '', '/admin/company.position/index', '1579145346', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('53', '员工管理', '1', '49', 'false', 'layui-icon', '1', 'layui-icon-diamond', '', '/admin/company.staff/index', '1579145520', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('54', '权限管理', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-senior', '', '/admin/index/okss', '1579145579', '127.0.0.1', '0', '', '', '120');
INSERT INTO `hhy_menu` VALUES ('55', '模快管理', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-component', '', '', '1579145718', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('56', '附件管理', '1', '55', 'false', 'layui-icon', '1', 'layui-icon-upload', '', '/admin/upload/index', '1579145745', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('57', '友情链接', '1', '55', 'false', 'layui-icon', '1', 'layui-icon-fonts-code', '', '/admin/friendlink/index', '1579145796', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('58', '定时任务', '1', '55', 'false', 'layui-icon', '1', 'layui-icon-radio', '', '/admin/queue.queue/index', '1579145868', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('59', '相册管理', '1', '55', 'false', 'layui-icon', '1', 'layui-icon-app', '', '/admin/album/index', '1579145936', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('60', '我的设置', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-username', '', '', '1579145996', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('61', '基本资料', '1', '60', 'false', 'layui-icon', '1', 'layui-icon-cellphone', '', '/admin/setting/index', '1579146032', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('62', '修改密码', '1', '60', 'false', 'layui-icon', '1', 'layui-icon-refresh-3', '', '/admin/setting/pass', '1579146045', '127.0.0.1', '0', '', '', '0');
INSERT INTO `hhy_menu` VALUES ('63', '组件管理', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-app', null, '', '1583122094', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('64', '图标选择器', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/pack/iconpick', '1583129681', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('65', '三级联动', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/pack/select', '1583129682', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('66', 'WangEditor', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/pack/wangEditor', '1583133956', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('67', '分步表单', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/pack/formStep', '1583134369', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('68', '表单选择', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/pack/choose', '1583135151', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('69', '内容管理', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-read', null, '', '1583224950', '127.0.0.1', '0', null, null, '780');
INSERT INTO `hhy_menu` VALUES ('70', '文章列表', '1', '69', 'false', 'layui-icon', '1', 'layui-icon-snowflake', null, '/admin/article/index', '1583225026', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('71', '文章栏目', '1', '69', 'false', 'layui-icon', '1', 'layui-icon-note', null, '/admin/category/index', '1583225079', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('72', '文章评论', '1', '69', 'false', 'layui-icon', '1', 'layui-icon-rate', null, '/admin/comment/index', '1583225121', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('73', '文章模型', '1', '69', 'false', 'layui-icon', '1', 'layui-icon-rate-solid', null, '/admin/model/index', '1583225175', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('74', '图集类型', '1', '69', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/picture/index', '1583389943', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('75', '视频上传', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-video', null, '/admin/pack/video', '1583975790', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('76', '配置管理', '1', '6', 'false', 'layui-icon', '1', 'layui-icon-rate-solid', null, '/admin/setting/setList', '1584169111', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('77', '钩子插件', '1', '0', 'false', 'layui-icon', '1', 'layui-icon-util', null, '', '1584501814', '127.0.0.1', '1584532881', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('78', '插件管理', '1', '77', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/addons/addons/index', '1584501868', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('79', '钩子管理', '1', '77', 'false', 'layui-icon', '1', 'layui-icon-rate-solid', null, '/addons/addons/hooks', '1584501916', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('80', '系统日志', '1', '22', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/log/index', '1585036868', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('81', '日志文件', '1', '22', 'false', 'layui-icon', '1', 'layui-icon-file-b', null, '/admin/log/logfile', '1585037057', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('82', 'Editor.md', '1', '63', 'false', 'layui-icon', '1', 'layui-icon-rate', null, '/admin/pack/uditmd', '1585101211', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('83', '百度熊掌推送', '1', '55', 'false', 'layui-icon', '1', 'layui-icon-vercode', null, '', '1585193756', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('84', '百度推送', '1', '83', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/push.baidu/index', '1585193818', '127.0.0.1', '0', null, null, '0');
INSERT INTO `hhy_menu` VALUES ('85', '熊掌号推送', '1', '83', 'false', 'layui-icon', '1', 'layui-icon-rate-half', null, '/admin/push.xzhang/index', '1585193845', '127.0.0.1', '0', null, null, '0');

-- ----------------------------
-- Table structure for hhy_models
-- ----------------------------
DROP TABLE IF EXISTS `hhy_models`;
CREATE TABLE `hhy_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '模型名称',
  `tablename` char(20) NOT NULL DEFAULT '' COMMENT '表名',
  `index_template` char(30) NOT NULL DEFAULT 'index' COMMENT '封面页模板',
  `list_template` char(30) NOT NULL DEFAULT 'list' COMMENT '列表模板',
  `show_template` char(30) NOT NULL DEFAULT 'show' COMMENT '详情页模板',
  `sort` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'asc 排序',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  `ip` varchar(80) DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模型表';

-- ----------------------------
-- Records of hhy_models
-- ----------------------------
INSERT INTO `hhy_models` VALUES ('1', '单页模型', 'page', 'index', 'list', 'show', '2', '1583305903', '0', '');
INSERT INTO `hhy_models` VALUES ('2', '文章模型', 'article', 'index', 'list', 'show', '1', '1583305903', '0', '');
INSERT INTO `hhy_models` VALUES ('3', '图集模型', 'picture', 'index', 'list', 'show', '3', '1583305903', '0', '');
INSERT INTO `hhy_models` VALUES ('4', '链接模型', 'link', 'index', 'list', 'show', '5', '1583305903', '0', '');
INSERT INTO `hhy_models` VALUES ('5', '下载模型', 'download', 'index', 'list', 'show', '4', '1583305903', '0', '');
INSERT INTO `hhy_models` VALUES ('6', '视频模型', 'video', 'index', 'list', 'show', '6', '1583305903', '0', '');

-- ----------------------------
-- Table structure for hhy_picture
-- ----------------------------
DROP TABLE IF EXISTS `hhy_picture`;
CREATE TABLE `hhy_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL COMMENT '栏目ID',
  `title` varchar(50) DEFAULT '' COMMENT '图片名称',
  `image_url` varchar(200) DEFAULT '' COMMENT '封面图片',
  `images` text COMMENT '图集地址json',
  `content` text COMMENT '详情',
  `keywords` varchar(100) DEFAULT '',
  `description` varchar(200) DEFAULT '' COMMENT '图片描述',
  `hits` int(11) NOT NULL DEFAULT '0' COMMENT '点击量',
  `is_recommend` tinyint(1) DEFAULT '0' COMMENT '是否推荐 0,1',
  `is_top` tinyint(4) DEFAULT '0' COMMENT '是否置顶 0,1',
  `is_show` tinyint(1) DEFAULT '0' COMMENT '是否显示 0不显示 1显示',
  `is_pwd` varchar(4) DEFAULT '0' COMMENT '是否需要密码  0不需要  不为0 就是密码',
  `comment_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论数',
  `url` varchar(200) DEFAULT '' COMMENT '地址',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(80) DEFAULT '',
  `delete_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  KEY `is_recommend` (`is_recommend`) USING BTREE,
  KEY `is_top` (`is_top`) USING BTREE,
  KEY `is_show` (`is_show`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='图集表';

-- ----------------------------
-- Records of hhy_picture
-- ----------------------------
INSERT INTO `hhy_picture` VALUES ('1', '4', '测试图集', '/uploads/images/20200305/0702534626bdf4480fa07bf26cd837c6.png', 'a:3:{i:0;s:61:\"/uploads/images/20200305/0702534626bdf4480fa07bf26cd837c6.png\";i:1;s:61:\"/uploads/images/20200305/fb7c5cc6e05706c6d1e6292436d11e22.png\";i:2;s:61:\"/uploads/images/20200305/fdc3790b988e284044d9d371c8cf3d42.png\";}', '&lt;p&gt;大大萨达萨达撒打算&lt;/p&gt;', '哈哈', '大傻傻哒', '10', '0', '0', '1', '0', '0', '/index/picture/show/id/1.html', '1583395748', '1583395748', '127.0.0.1', '0');

-- ----------------------------
-- Table structure for hhy_recod
-- ----------------------------
DROP TABLE IF EXISTS `hhy_recod`;
CREATE TABLE `hhy_recod` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(25) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `browser` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COMMENT='访问者记录';

-- ----------------------------
-- Records of hhy_recod
-- ----------------------------
INSERT INTO `hhy_recod` VALUES ('2', '222.247.164.28', '湖南省长沙市', '1582874307', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('3', '113.99.18.227', '广东省广州市', '1582876345', 'Chrome/76.0.3809.100');
INSERT INTO `hhy_recod` VALUES ('4', '27.10.194.71', '重庆市重庆市', '1582878511', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('5', '222.247.164.28', '湖南省长沙市', '1582878966', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('6', '222.186.128.215', '江苏省镇江市', '1582879150', 'Chrome/82.0.4061.0');
INSERT INTO `hhy_recod` VALUES ('7', '222.247.164.28', '湖南省长沙市', '1582879247', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('8', '115.63.13.165', '河南省信阳市', '1582880860', 'Chrome/74.0.3729.108');
INSERT INTO `hhy_recod` VALUES ('9', '182.132.193.139', '四川省眉山市', '1582881794', 'Chrome/69.0.3497.100');
INSERT INTO `hhy_recod` VALUES ('10', '120.227.210.24', '湖南省张家界市', '1582882047', 'Chrome/69.0.3497.100');
INSERT INTO `hhy_recod` VALUES ('11', '171.114.109.130', '湖北省宜昌市', '1582882146', 'Firefox/73.0');
INSERT INTO `hhy_recod` VALUES ('12', '27.223.106.34', '山东省青岛市', '1582885015', 'Chrome/79.0.3945.130');
INSERT INTO `hhy_recod` VALUES ('13', '27.17.233.103', '湖北省武汉市', '1582885339', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('14', '120.244.152.128', '北京市北京市', '1582889130', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('15', '125.93.231.38', '广东省东莞市', '1582889339', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('16', '106.122.186.65', '福建省厦门市', '1582889391', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('17', '222.160.38.225', '吉林省四平市', '1582889851', 'Chrome/55.0.2883.87');
INSERT INTO `hhy_recod` VALUES ('18', '222.160.38.225', '吉林省四平市', '1582890687', 'Chrome/55.0.2883.87');
INSERT INTO `hhy_recod` VALUES ('19', '106.114.68.51', '河北省石家庄市', '1582891794', 'Chrome/74.0.3729.131');
INSERT INTO `hhy_recod` VALUES ('20', '111.8.160.114', '湖南省娄底市', '1582891872', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('21', '113.246.152.132', '湖南省长沙市', '1582894078', 'Chrome/80.0.3987.100');
INSERT INTO `hhy_recod` VALUES ('22', '113.246.153.58', '湖南省长沙市', '1582895183', 'Firefox/73.0');
INSERT INTO `hhy_recod` VALUES ('23', '223.88.129.26', '河南省洛阳市', '1582895974', 'Chrome/77.0.3865.90');
INSERT INTO `hhy_recod` VALUES ('24', '118.254.107.177', '湖南省张家界市', '1582896847', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('25', '27.8.186.103', '重庆市重庆市', '1582897074', 'Chrome/80.0.3987.87');
INSERT INTO `hhy_recod` VALUES ('26', '223.73.60.144', '广东省广州市', '1582897153', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('27', '123.129.125.2', '山东省东营市', '1582897351', 'Chrome/69.0.3497.81');
INSERT INTO `hhy_recod` VALUES ('28', '223.88.55.177', '河南省郑州市', '1582897418', 'Chrome/74.0.3729.131');
INSERT INTO `hhy_recod` VALUES ('29', '58.255.230.124', '广东省茂名市', '1582897525', 'Firefox/73.0');
INSERT INTO `hhy_recod` VALUES ('30', '219.142.146.188', '北京市北京市', '1582897694', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('31', '117.140.161.143', '广西壮族自治区百色市', '1582903070', 'Chrome/62.0.3202.84');
INSERT INTO `hhy_recod` VALUES ('32', '219.143.128.133', '北京市北京市', '1582905718', 'Chrome/76.0.3809.100');
INSERT INTO `hhy_recod` VALUES ('33', '119.123.49.91', '广东省深圳市', '1582906402', 'Chrome/66.0.3359.181');
INSERT INTO `hhy_recod` VALUES ('34', '123.182.88.207', '河北省承德市', '1582907419', 'Chrome/61.0.3163.79');
INSERT INTO `hhy_recod` VALUES ('35', '112.48.6.82', '福建省厦门市', '1582908685', 'unkown');
INSERT INTO `hhy_recod` VALUES ('36', '182.200.209.73', '辽宁省沈阳市', '1582909526', 'unkown');
INSERT INTO `hhy_recod` VALUES ('37', '223.91.202.24', '河南省新乡市', '1582913190', 'Chrome/81.0.4044.26');
INSERT INTO `hhy_recod` VALUES ('38', '61.179.207.195', '山东省威海市', '1582937486', 'Chrome/70.0.3538.25');
INSERT INTO `hhy_recod` VALUES ('39', '117.30.208.134', '福建省厦门市', '1582937598', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('40', '113.132.11.81', '陕西省西安市', '1582937957', 'Chrome/63.0.3239.26');
INSERT INTO `hhy_recod` VALUES ('41', '123.122.166.35', '北京市北京市', '1582938985', 'Chrome/61.0.3163.79');
INSERT INTO `hhy_recod` VALUES ('42', '125.111.141.194', '浙江省宁波市', '1582940571', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('43', '43.225.208.151', '黑龙江省哈尔滨市', '1582941512', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('44', '116.21.144.129', '广东省广州市', '1582942984', 'Chrome/80.0.3987.122');
INSERT INTO `hhy_recod` VALUES ('45', '117.136.52.29', '湖北省武汉市', '1582946611', 'Chrome/70.0.3538.64');
INSERT INTO `hhy_recod` VALUES ('46', '183.192.235.237', '上海市上海市', '1582946644', 'Chrome/67.0.3396.99');
INSERT INTO `hhy_recod` VALUES ('47', '117.136.52.29', '湖北省武汉市', '1582946773', 'Chrome/79.0.3945.130');
INSERT INTO `hhy_recod` VALUES ('48', '14.116.144.199', '广东省广州市', '1582946839', 'Chrome/57.0.2987.108');
INSERT INTO `hhy_recod` VALUES ('49', '110.187.205.238', '四川省泸州市', '1582946924', 'unkown');
INSERT INTO `hhy_recod` VALUES ('50', '110.187.205.238', '四川省泸州市', '1582947024', 'Chrome/77.0.3865.90');
INSERT INTO `hhy_recod` VALUES ('51', '222.247.64.196', '湖南省长沙市', '1582947553', 'Chrome/74.0.3729.131');
INSERT INTO `hhy_recod` VALUES ('52', '112.3.206.71', '江苏省苏州市', '1582947620', 'Chrome/76.0.3809.87');
INSERT INTO `hhy_recod` VALUES ('53', '220.113.127.11', '湖北省武汉市', '1582951806', 'Chrome/80.0.3987.100');
INSERT INTO `hhy_recod` VALUES ('54', '183.197.184.193', '河北省张家口市', '1582951938', 'Chrome/69.0.3497.100');
INSERT INTO `hhy_recod` VALUES ('55', '119.39.101.225', '湖南省长沙市', '1582956633', 'Chrome/75.0.3770.100');
INSERT INTO `hhy_recod` VALUES ('56', '127.0.0.1', '', '1583743669', 'Chrome/80.0.3987.132');
INSERT INTO `hhy_recod` VALUES ('57', '127.0.0.1', '', '1583809039', 'Chrome/80.0.3987.132');
INSERT INTO `hhy_recod` VALUES ('58', '127.0.0.1', '', '1584165357', 'Chrome/78.0.3904.108');
INSERT INTO `hhy_recod` VALUES ('59', '127.0.0.1', '', '1584348707', 'Chrome/80.0.3987.132');
INSERT INTO `hhy_recod` VALUES ('60', '127.0.0.1', '', '1584512754', 'Chrome/80.0.3987.132');
INSERT INTO `hhy_recod` VALUES ('61', '127.0.0.1', '', '1584669415', 'Chrome/80.0.3987.132');
INSERT INTO `hhy_recod` VALUES ('62', '127.0.0.1', '', '1585021356', 'Chrome/80.0.3987.149');
INSERT INTO `hhy_recod` VALUES ('63', '127.0.0.1', '', '1585032511', 'Chrome/80.0.3987.149');
INSERT INTO `hhy_recod` VALUES ('64', '127.0.0.1', '', '1585099790', 'Chrome/80.0.3987.149');

-- ----------------------------
-- Table structure for hhy_role
-- ----------------------------
DROP TABLE IF EXISTS `hhy_role`;
CREATE TABLE `hhy_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `rule` text CHARACTER SET utf8 COMMENT '权限菜单id',
  PRIMARY KEY (`id`),
  KEY `s` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='角色权限表';

-- ----------------------------
-- Records of hhy_role
-- ----------------------------
INSERT INTO `hhy_role` VALUES ('1', '超级管理员', '1562126200', '0', '127.0.0.1', '0', '1', 'all');
INSERT INTO `hhy_role` VALUES ('2', '测试', '1578649993', '0', '127.0.0.1', '0', '1', 'a:3:{i:1;s:1:\"6\";i:2;s:1:\"7\";i:3;s:1:\"8\";}');

-- ----------------------------
-- Table structure for hhy_setting
-- ----------------------------
DROP TABLE IF EXISTS `hhy_setting`;
CREATE TABLE `hhy_setting` (
  `key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` text CHARACTER SET utf8,
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='设置表';

-- ----------------------------
-- Records of hhy_setting
-- ----------------------------
INSERT INTO `hhy_setting` VALUES ('admin_log', '1', '开启后台日志，1开启2关闭');
INSERT INTO `hhy_setting` VALUES ('ext', 'jpg,png,rar,zip,pdf,mp4,doc,docx,xls,xlsx,jpeg,gif,txt', null);
INSERT INTO `hhy_setting` VALUES ('foot', 'Copyright ©2019-©2022 <a href=\"http://www.luckyhhy.cn\">LuckyHHY</a> All Rights Reserved                                                                                                                                                                                                                                                                                                                                                                                        ', null);
INSERT INTO `hhy_setting` VALUES ('forbid_ip', null, null);
INSERT INTO `hhy_setting` VALUES ('home_title', 'JrkAdmin', null);
INSERT INTO `hhy_setting` VALUES ('icp', '湘ICP备190144483号', null);
INSERT INTO `hhy_setting` VALUES ('mail_from_name', 'jackhhy', null);
INSERT INTO `hhy_setting` VALUES ('mail_smtp_host', 'smtp.qq.com', null);
INSERT INTO `hhy_setting` VALUES ('mail_smtp_pass', 'wewdsadasdasd', null);
INSERT INTO `hhy_setting` VALUES ('mail_smtp_port', '465', null);
INSERT INTO `hhy_setting` VALUES ('mail_smtp_user', 'jackhhy520@qq.com', null);
INSERT INTO `hhy_setting` VALUES ('mail_type', '1', null);
INSERT INTO `hhy_setting` VALUES ('mail_verify_type', '2', null);
INSERT INTO `hhy_setting` VALUES ('meta', 'JRKadmin,JRKadmin网站后台系统,后台系统下载,thinkphp后台管理系统,layui后台系统下载', null);
INSERT INTO `hhy_setting` VALUES ('meta_describe', 'JRKadmin，顾名思义，很赞的后台管理系统，它是一款基于Layui+TP5.1的轻量级扁平化且完全免费开源的网站后台管理系统，适合中小型CMS后台系统。', null);
INSERT INTO `hhy_setting` VALUES ('qiniu_access_key', '用你自己的吧', null);
INSERT INTO `hhy_setting` VALUES ('qiniu_bucket', 'https', null);
INSERT INTO `hhy_setting` VALUES ('qiniu_domain', '用你自己的吧', null);
INSERT INTO `hhy_setting` VALUES ('qiniu_is_https', 'https', null);
INSERT INTO `hhy_setting` VALUES ('qiniu_region', '华东', null);
INSERT INTO `hhy_setting` VALUES ('qiniu_secret_key', '用你自己的吧', null);
INSERT INTO `hhy_setting` VALUES ('site_cache', '4234234', null);
INSERT INTO `hhy_setting` VALUES ('site_domain', 'http://www.luckyhhy.cn', null);
INSERT INTO `hhy_setting` VALUES ('site_name', 'JrkAdmin', null);
INSERT INTO `hhy_setting` VALUES ('site_open', '2', null);
INSERT INTO `hhy_setting` VALUES ('size', '15678000', null);
INSERT INTO `hhy_setting` VALUES ('storage_oss_bucket', 'cuci-mytest', null);
INSERT INTO `hhy_setting` VALUES ('storage_oss_domain', '用你自己的', null);
INSERT INTO `hhy_setting` VALUES ('storage_oss_endpoint', 'oss-cn-hangzhou.aliyuncs.com', null);
INSERT INTO `hhy_setting` VALUES ('storage_oss_is_https', 'http', null);
INSERT INTO `hhy_setting` VALUES ('storage_oss_keyid', '用你自己的', null);
INSERT INTO `hhy_setting` VALUES ('storage_oss_secret', '用你自己的', null);
INSERT INTO `hhy_setting` VALUES ('upload_image_ext', 'jpg|png|gif|bmp|jpeg', null);
INSERT INTO `hhy_setting` VALUES ('upload_image_max', '30', '图片最多上传张数');
INSERT INTO `hhy_setting` VALUES ('upload_image_size', '10240', '图片最大上传大小(默认：10MB)');
INSERT INTO `hhy_setting` VALUES ('upload_type', '1', null);
INSERT INTO `hhy_setting` VALUES ('upload_video_ext', 'mp4|avi|mov|rmvb|flv', '视频后缀限制');
INSERT INTO `hhy_setting` VALUES ('upload_video_max', '3', '视频最大上传数量限制');
INSERT INTO `hhy_setting` VALUES ('upload_video_size', '102400', '视频最大上传文件大小(默认：100MB)');
INSERT INTO `hhy_setting` VALUES ('wechat_appid', 'wx60a43', null);
INSERT INTO `hhy_setting` VALUES ('wechat_appsecret', '3d4b42868d183d60b', null);
INSERT INTO `hhy_setting` VALUES ('wechat_encodingaeskey', '', null);
INSERT INTO `hhy_setting` VALUES ('wechat_mch_id', '13', null);
INSERT INTO `hhy_setting` VALUES ('wechat_mch_key', 'A82DC5B', null);
INSERT INTO `hhy_setting` VALUES ('wechat_mch_ssl_cer', '09b29a.pem', null);
INSERT INTO `hhy_setting` VALUES ('wechat_mch_ssl_key', '294f283d.pem', null);
INSERT INTO `hhy_setting` VALUES ('wechat_mch_ssl_p12', '718182d/1bc857ee646aa15d.p12', null);
INSERT INTO `hhy_setting` VALUES ('wechat_mch_ssl_type', 'p12', null);
INSERT INTO `hhy_setting` VALUES ('wechat_push_url', '消息推送地址：http://127.0.0.1:8000/wechat/api.push', null);
INSERT INTO `hhy_setting` VALUES ('wechat_thr_appid', 'wx60a43dd8', null);
INSERT INTO `hhy_setting` VALUES ('wechat_thr_appkey', '5caf4b0727f6e4', null);
INSERT INTO `hhy_setting` VALUES ('wechat_thr_appurl', '消息推送地址：http://127.0.0.1:2314/wechat/api.push', null);
INSERT INTO `hhy_setting` VALUES ('wechat_token', 'mytoken', null);
INSERT INTO `hhy_setting` VALUES ('wechat_type', 'thr', null);
INSERT INTO `hhy_setting` VALUES ('xzappid', 'a', '熊掌号APPID');
INSERT INTO `hhy_setting` VALUES ('xztoken', 'aa', '熊掌号token');
INSERT INTO `hhy_setting` VALUES ('zz_site', 'www.luckyhhy.cn', '百度站长配置');
INSERT INTO `hhy_setting` VALUES ('zz_token', 'ixg7nB9MHI5bK', '百度站长token');

-- ----------------------------
-- Table structure for hhy_sys_queue
-- ----------------------------
DROP TABLE IF EXISTS `hhy_sys_queue`;
CREATE TABLE `hhy_sys_queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '事件类型',
  `title` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '事件标题',
  `content` text CHARACTER SET utf8 NOT NULL COMMENT '事件内容',
  `schedule` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Crontab格式',
  `sleep` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '延迟秒数执行',
  `maximums` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大执行次数 0为不限',
  `executes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '已经执行的次数',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `begintime` int(10) DEFAULT '0' COMMENT '开始时间',
  `endtime` int(10) DEFAULT '0' COMMENT '结束时间',
  `executetime` int(10) DEFAULT '0' COMMENT '最后执行时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` enum('completed','expired','hidden','normal') CHARACTER SET utf8 NOT NULL DEFAULT 'normal' COMMENT '状态',
  `ip` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='定时任务表';

-- ----------------------------
-- Records of hhy_sys_queue
-- ----------------------------
INSERT INTO `hhy_sys_queue` VALUES ('1', 'url', '测试', 'www.baidu.com', '5 * * * *', '0', '0', '0', '1585185045', '0', '1585152000', '1585238400', '0', '0', 'normal', '127.0.0.1');

-- ----------------------------
-- Table structure for hhy_sys_queue_log
-- ----------------------------
DROP TABLE IF EXISTS `hhy_sys_queue_log`;
CREATE TABLE `hhy_sys_queue_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `crontab_id` int(10) unsigned DEFAULT '0' COMMENT '任务ID',
  `executetime` int(10) DEFAULT NULL COMMENT '执行时间',
  `completetime` int(10) DEFAULT NULL COMMENT '结束时间',
  `content` text CHARACTER SET utf8 COMMENT '执行结果',
  `status` enum('success','failure') CHARACTER SET utf8 DEFAULT 'failure' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `crontab_id` (`crontab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='定时任务日志表';

-- ----------------------------
-- Records of hhy_sys_queue_log
-- ----------------------------

-- ----------------------------
-- Table structure for hhy_upload
-- ----------------------------
DROP TABLE IF EXISTS `hhy_upload`;
CREATE TABLE `hhy_upload` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_size` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `filename` varchar(255) CHARACTER SET utf8 NOT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `file_md5` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `file_sha1` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `suffix` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `up_type` tinyint(2) NOT NULL COMMENT '1 本地 2七牛 3oss',
  `ext` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='附件表';

-- ----------------------------
-- Records of hhy_upload
-- ----------------------------
INSERT INTO `hhy_upload` VALUES ('1', '135036', '1582970466', '1', '095535_8d7dac8e_1513275.png', '/uploads/sucai_img/20200229/9a2ebc15e169c25760b9e4bf8161f6dd.png', 'dc548d066b16a203db3517e8a12c7d60', '2bda5cbee31a0cd90ab8f8f3694cc4ec25b52013', 'file', '1', 'png');
INSERT INTO `hhy_upload` VALUES ('2', '215', '1583044762', '1', 'git推送代码笔记.txt', '/uploads/sucai_file/20200301/2cc20fbc1360f09f722431aa1ff862ff.txt', '2051bd6c598c63297a67fba56ac54466', 'db3cc754d3355f5ea418ec81f19798018fe5eae8', 'file', '1', 'txt');

-- ----------------------------
-- Table structure for hhy_users
-- ----------------------------
DROP TABLE IF EXISTS `hhy_users`;
CREATE TABLE `hhy_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qq` int(12) unsigned NOT NULL COMMENT 'qq 号码',
  `user_num` varchar(152) CHARACTER SET utf8 NOT NULL COMMENT '用户编号',
  `username` varchar(55) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户名称',
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '头像地址',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) DEFAULT '0',
  `ip` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(3) DEFAULT '1' COMMENT '1 正常 2拉黑',
  `zan` int(11) DEFAULT '0' COMMENT '集赞数',
  `fen` int(11) DEFAULT '0' COMMENT '积分',
  PRIMARY KEY (`id`),
  KEY `s` (`status`) USING BTREE,
  KEY `qq` (`qq`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of hhy_users
-- ----------------------------
INSERT INTO `hhy_users` VALUES ('1', '1668862539', '', 'BY-贺老大', 'http://q.qlogo.cn/headimg_dl?dst_uin=1668862539&spec=100', '1565945676', '0', '', '1', '0', '5');
INSERT INTO `hhy_users` VALUES ('2', '2237696522', '', '杰瑞克', 'http://q.qlogo.cn/headimg_dl?dst_uin=1668862539&spec=100', '1565945676', '0', '', '1', '1', '10');
INSERT INTO `hhy_users` VALUES ('4', '123456', '', '腾讯视频', 'http://q.qlogo.cn/headimg_dl?dst_uin=123456&spec=100', '1565945676', '0', '127.0.0.1', '1', '0', '0');
INSERT INTO `hhy_users` VALUES ('6', '11111', '', '佩玲月韵', 'http://q.qlogo.cn/headimg_dl?dst_uin=11111&spec=100', '1566191360', '0', '127.0.0.1', '1', '0', '0');
INSERT INTO `hhy_users` VALUES ('7', '222222', '', 'MicroCat', 'http://q.qlogo.cn/headimg_dl?dst_uin=222222&spec=100', '1566193716', '0', '127.0.0.1', '1', '0', '0');
INSERT INTO `hhy_users` VALUES ('8', '2222222', '', '游客', 'http://q.qlogo.cn/headimg_dl?dst_uin=2222222&spec=100', '1566358798', '0', '127.0.0.1', '1', '0', '0');
INSERT INTO `hhy_users` VALUES ('9', '2147483647', '', '游客', 'http://q.qlogo.cn/headimg_dl?dst_uin=2147483647&spec=100', '1566359102', '0', '127.0.0.1', '1', '0', '0');

-- ----------------------------
-- Table structure for hhy_wx_account
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_account`;
CREATE TABLE `hhy_wx_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `store_id` int(11) NOT NULL DEFAULT '1' COMMENT 'uid',
  `wxname` varchar(60) NOT NULL DEFAULT '' COMMENT '公众号名称',
  `aeskey` varchar(256) NOT NULL DEFAULT '' COMMENT 'aeskey',
  `encode` varchar(100) NOT NULL DEFAULT '0' COMMENT 'encode',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'appid',
  `app_secret` varchar(50) NOT NULL DEFAULT '' COMMENT 'appsecret',
  `origin_id` varchar(64) NOT NULL DEFAULT '' COMMENT '公众号原始ID',
  `weixin` char(64) NOT NULL COMMENT '微信号',
  `logo` char(255) NOT NULL COMMENT '头像地址',
  `token` char(255) DEFAULT '' COMMENT 'token',
  `w_token` varchar(150) NOT NULL DEFAULT '' COMMENT '微信对接token',
  `related` varchar(200) NOT NULL DEFAULT '' COMMENT '微信对接地址',
  `create_time` int(11) NOT NULL COMMENT 'create_time',
  `update_time` int(11) DEFAULT '0' COMMENT 'updatetime',
  `tplcontentid` varchar(2) NOT NULL DEFAULT '' COMMENT '内容模版ID',
  `share_ticket` varchar(150) NOT NULL DEFAULT '' COMMENT '分享ticket',
  `share_dated` char(15) DEFAULT '' COMMENT 'share_dated',
  `authorizer_access_token` varchar(200) NOT NULL DEFAULT '' COMMENT 'authorizer_access_token',
  `authorizer_refresh_token` varchar(200) NOT NULL DEFAULT '' COMMENT 'authorizer_refresh_token',
  `authorizer_expires` char(10) DEFAULT '' COMMENT 'authorizer_expires',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型 1 普通订阅号2认证订阅号 3普通服务号 4认证服务号/认证媒体/政府订阅号',
  `web_access_token` varchar(200) DEFAULT '' COMMENT '网页授权token',
  `web_refresh_token` varchar(200) DEFAULT '' COMMENT 'web_refresh_token',
  `web_expires` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间',
  `qr` varchar(200) NOT NULL DEFAULT '' COMMENT 'qr',
  `menu_config` mediumtext COMMENT '菜单',
  `status` tinyint(1) DEFAULT '0' COMMENT '微信接入状态,0待接入1已接入',
  PRIMARY KEY (`id`),
  KEY `uid` (`store_id`) USING BTREE,
  KEY `uid_2` (`store_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信公共帐号';

-- ----------------------------
-- Records of hhy_wx_account
-- ----------------------------

-- ----------------------------
-- Table structure for hhy_wx_fans
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_fans`;
CREATE TABLE `hhy_wx_fans` (
  `fans_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '粉丝ID',
  `wx_aid` int(11) DEFAULT NULL COMMENT '微信账户id',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '会员编号ID',
  `source_uid` int(11) NOT NULL DEFAULT '0' COMMENT '推广人uid',
  `store_id` int(11) NOT NULL DEFAULT '1' COMMENT '店铺ID',
  `nickname` varchar(255) NOT NULL COMMENT '昵称',
  `nickname_encode` varchar(255) DEFAULT '',
  `headimgurl` varchar(500) NOT NULL DEFAULT '' COMMENT '头像',
  `sex` smallint(6) NOT NULL DEFAULT '1' COMMENT '性别',
  `language` varchar(20) NOT NULL DEFAULT '' COMMENT '用户语言',
  `country` varchar(60) NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(255) NOT NULL DEFAULT '' COMMENT '省',
  `city` varchar(255) NOT NULL DEFAULT '' COMMENT '城市',
  `district` varchar(255) NOT NULL DEFAULT '' COMMENT '行政区/县',
  `openid` varchar(255) NOT NULL DEFAULT '' COMMENT '用户的标识，对当前公众号唯一     用户的唯一身份ID',
  `unionid` varchar(255) NOT NULL DEFAULT '' COMMENT '粉丝unionid',
  `groupid` int(11) NOT NULL DEFAULT '0' COMMENT '粉丝所在组id',
  `subscribe` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否订阅',
  `subscribe_scene` varchar(50) DEFAULT NULL COMMENT '订阅场景',
  `remark` varchar(255) NOT NULL COMMENT '备注',
  `tag` varchar(200) DEFAULT NULL COMMENT '标签',
  `tagid_list` varchar(255) DEFAULT NULL COMMENT '标签列表',
  `subscribe_time` int(11) DEFAULT '0' COMMENT '订阅时间',
  `unsubscribe_time` int(11) DEFAULT '0' COMMENT '解订阅时间',
  `qr_scene` varchar(255) DEFAULT NULL COMMENT '二维码扫码场景（开发者自定义）',
  `qr_scene_str` varchar(255) DEFAULT NULL COMMENT '二维码扫码场景描述（开发者自定义）',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `update_time` int(11) DEFAULT '0' COMMENT '粉丝信息最后更新时间',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`fans_id`),
  KEY `openid` (`openid`(191)),
  KEY `unionid` (`unionid`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='微信公众号获取粉丝列表';

-- ----------------------------
-- Records of hhy_wx_fans
-- ----------------------------
INSERT INTO `hhy_wx_fans` VALUES ('1', '1', '0', '0', '1', '🐘 心之所向🐘', '\"\\ud83d\\udc18 \\u5fc3\\u4e4b\\u6240\\u5411\\ud83d\\udc18\"', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM4VFiaYnBD77jqvXaG55kz8cYgynjUAic5oNcrjkicjIGvVVyRYfLsiceojIlI709OKWPAQr95E2y2Ick6jSHSrIJXgtcn1VnDM4qE/132', '1', 'zh_CN', '中国', '湖南', '衡阳', '', 'oBSasxCSibhs0U_O8d1QCLRR6woQ', '', '2', '1', 'ADD_SCENE_QR_CODE', '', '星标组', '[2]', '1568970767', '0', '0', '', '1', '1572230913', '1567909800');
INSERT INTO `hhy_wx_fans` VALUES ('2', '1', '0', '0', '1', '少年智力开发报订阅', '\"\\u5c11\\u5e74\\u667a\\u529b\\u5f00\\u53d1\\u62a5\\u8ba2\\u9605\"', 'http://thirdwx.qlogo.cn/mmopen/7jOTIafB9k4w5h73kjDCf0o0IXjb7tNuJHk45lY9ZopsqS4rsQ5UxkAgvOqe49UESQyiaHp0jG7u3p1WhiaHpm7g/132', '1', 'zh_CN', '中国', '河北', '石家庄', '', 'oBSasxDCwYJ4QlFRgSbi-SZktfZs', '', '2', '1', 'ADD_SCENE_QR_CODE', '', '其他', '[2]', '1570784081', '0', '0', '', '1', '1572230913', '1571531137');

-- ----------------------------
-- Table structure for hhy_wx_material
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_material`;
CREATE TABLE `hhy_wx_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '微信公众号素材',
  `store_id` int(11) NOT NULL DEFAULT '1',
  `wx_aid` int(11) DEFAULT NULL,
  `media_id` varchar(64) DEFAULT '' COMMENT '微信媒体id',
  `file_name` varchar(255) DEFAULT NULL COMMENT '视频文件名',
  `media_url` varchar(255) DEFAULT NULL,
  `local_cover` varchar(255) NOT NULL DEFAULT ' ',
  `type` varchar(10) NOT NULL COMMENT '图片（image）、视频（video）、语音 （voice）、图文（news）音乐（music）',
  `des` varchar(150) DEFAULT ' ' COMMENT '视频描述',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(10) unsigned DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COMMENT='微信公众号素材';

-- ----------------------------
-- Records of hhy_wx_material
-- ----------------------------
INSERT INTO `hhy_wx_material` VALUES ('1', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vWSQRum5u4VDUPiUFigwiJ8', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOYicrQW6efiaJakGletiaBalPjFbNX5kU9aichjf05tqZItDZnOa1aDW7wPG2vr5QbzCRnTHh8a9QKJvg/0?wx_fmt=png', ' ', 'news', ' ', '1584675779', null);
INSERT INTO `hhy_wx_material` VALUES ('3', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vW3JDaKiGYEHvSUt9e_7aJc', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOYo2yxoPGwSSLeww3IhmICIcoozvA6bCH9IiaBq0S2oGYj5U0iarHiaoszKhoV1TeYD5EMpk7NFu3RKw/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('4', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vY0wIpIk39IWcXdijBIsYJI', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOb9LnhLnhwPeEmdrVWJfWhCllbUHQLsiaWH6Hqk2gjYpv2gGst6h1icxC2WpVEoReZu0lh6FzPOCaIA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('5', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vWVICJi4XQbWqK-hBcs5ZIg', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaA9lwJVV1cB6jQHfphjgX3MrSH1icemwKcC6Ricf8qvXMXTT4gRASoFZBRaeSy1GE2zQbbofPOiawrg/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('6', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vbmcS_4rhuo_JE_huk2kaVY', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOZQM49Dibge0pBd5tCrxwXyT0uiaVXhSyclSRk3VIejsrvMADEvdcibyTDObXAUMYSibibuAagdss9NOvg/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('7', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vd3ujeY0Ji6pS77T5FmLmPM', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('8', '1', '1', 'Mkk-XekVsp2Cvr5VktS-veQTSd2RRXXReE12K1apxzc', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('9', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vbD1F99PFg_0kNE3PqENxNA', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('10', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vYFbr7trtHhNC-9z27bieGQ', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('11', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vcZwJS58dQdPGEszNRpK-7k', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('12', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vTgb71_DxIQnQiOqLy0CR_I', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('13', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vVJ9RXttHN6bMdzzGEG6rFw', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('14', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vYfhRQRYALV9yCysSS3R4lQ', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('15', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vdUAAQ9krKrHTD0JmAmnjAM', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('16', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vTPw2tiAlBV6u_6T3_T5Lwo', null, 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('17', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vVk5bkYwAR6I4K2PNmJsTNw', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7phlb1nGCCBOl7H62KYibhyn6ibco65rsP4gldTxiaiaT3EUnXVxynUTtZYw/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('18', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vdX-T_IlHKN9ZbEhC9wUbyw', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('19', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vWP7rc7VegzQsLtPkeFgylE', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);
INSERT INTO `hhy_wx_material` VALUES ('20', '1', '1', 'Mkk-XekVsp2Cvr5VktS-vdHp6UuWkpSXJ9b4oCswPZ8', null, 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', ' ', 'news', ' ', '1584676326', null);

-- ----------------------------
-- Table structure for hhy_wx_material_info
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_material_info`;
CREATE TABLE `hhy_wx_material_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `store_id` int(11) NOT NULL DEFAULT '1',
  `wx_aid` int(11) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `thumb_media_id` varchar(100) DEFAULT NULL COMMENT '	图文消息的封面图片素材id（必须是永久mediaID）',
  `local_cover` varchar(255) DEFAULT NULL,
  `cover` varchar(200) NOT NULL COMMENT '图文消息封面',
  `title` varchar(100) DEFAULT NULL,
  `author` varchar(50) NOT NULL COMMENT '作者',
  `show_cover` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示封面',
  `digest` text COMMENT '图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空',
  `content` text NOT NULL COMMENT '正文',
  `url` varchar(255) NOT NULL COMMENT '图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL',
  `content_source_url` varchar(200) NOT NULL DEFAULT '' COMMENT '图文消息的原文地址，即点击“阅读原文”后的URL',
  `need_open_comment` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Uint32 是否打开评论，0不打开，1打开',
  `only_fans_can_comment` tinyint(1) DEFAULT '1' COMMENT 'Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序号',
  `hits` int(11) NOT NULL DEFAULT '0' COMMENT '阅读次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='素材详细';

-- ----------------------------
-- Records of hhy_wx_material_info
-- ----------------------------
INSERT INTO `hhy_wx_material_info` VALUES ('2', '1', '1', '3', 'Mkk-XekVsp2Cvr5VktS-vb8EW7F9dKrI_4QKEP7G_sU', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOYo2yxoPGwSSLeww3IhmICIcoozvA6bCH9IiaBq0S2oGYj5U0iarHiaoszKhoV1TeYD5EMpk7NFu3RKw/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOYo2yxoPGwSSLeww3IhmICIcoozvA6bCH9IiaBq0S2oGYj5U0iarHiaoszKhoV1TeYD5EMpk7NFu3RKw/0?wx_fmt=jpeg', '这是一个什么样的标题？', '天下太平', '1', '这是一个什么样的标题？', '<p>这是一个什么样的标题？</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000171&idx=1&sn=60a8561e6e2e2305eebe415afa2ce9b6&chksm=6c9290c15be519d7b5a6230a3401f71df789ce708181e7308d57144df5bea0b1c5a9e556eea0#rd', 'http://www.baidu.com', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('3', '1', '1', '4', 'Mkk-XekVsp2Cvr5VktS-vdDYVsTdpxAcGUKCc0-AAxA', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOb9LnhLnhwPeEmdrVWJfWhCllbUHQLsiaWH6Hqk2gjYpv2gGst6h1icxC2WpVEoReZu0lh6FzPOCaIA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOb9LnhLnhwPeEmdrVWJfWhCllbUHQLsiaWH6Hqk2gjYpv2gGst6h1icxC2WpVEoReZu0lh6FzPOCaIA/0?wx_fmt=png', '测试', '111', '1', '222', '<p>222</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000144&idx=1&sn=2750f11732b61864c6a266ef31e01188&chksm=6c9290fa5be519ec5a51e072cca78b393a11eb888488cc5faec0bcca9982ca1ac1b5f658146b#rd', 'http://111', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('4', '1', '1', '5', 'Mkk-XekVsp2Cvr5VktS-vWW0VUAbeG1cdiC93mjgy-I', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaA9lwJVV1cB6jQHfphjgX3MrSH1icemwKcC6Ricf8qvXMXTT4gRASoFZBRaeSy1GE2zQbbofPOiawrg/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaA9lwJVV1cB6jQHfphjgX3MrSH1icemwKcC6Ricf8qvXMXTT4gRASoFZBRaeSy1GE2zQbbofPOiawrg/0?wx_fmt=jpeg', '测试', '111', '1', '111', '<p>11</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000138&idx=1&sn=def85018e879cea09892d1613648330e&chksm=6c9290e05be519f6f8976c2336221a7ac4df098f14029c22db04f15cd7c3067bdc8bb918f03d#rd', 'http://111', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('5', '1', '1', '6', 'Mkk-XekVsp2Cvr5VktS-vR5a2bcksis8mygt_wiLVHs', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOZQM49Dibge0pBd5tCrxwXyT0uiaVXhSyclSRk3VIejsrvMADEvdcibyTDObXAUMYSibibuAagdss9NOvg/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOZQM49Dibge0pBd5tCrxwXyT0uiaVXhSyclSRk3VIejsrvMADEvdcibyTDObXAUMYSibibuAagdss9NOvg/0?wx_fmt=jpeg', 'title打算', '啧啧啧', '0', '阿斯达十大', '<p>阿萨德阿萨德阿萨德</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000135&idx=1&sn=39c8d23d473df23a70e17a21ea5929ca&chksm=6c9290ed5be519fb7fe366b2b55ed492596b453a858d018497182c17b3e7405d38936ea1cc92#rd', 'http://啊啊啊', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('6', '1', '1', '7', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000127&idx=1&sn=3647860e9fa3d78ab3200bd512437b24&chksm=6c9290155be51903506f1c1273ec2f045cfaa2f8235e5cc4e6cc4f18db5c253ea79a8f818664#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('7', '1', '1', '8', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000126&idx=1&sn=9b690c7e912b61e32c6e3b0e134a0c16&chksm=6c9290145be519022f4d3957f8fd4d94d61efc835cf45d7391e772dd1023da5db52912001da5#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('8', '1', '1', '9', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000125&idx=1&sn=854e041dfbe35290028977a97656fa87&chksm=6c9290175be5190135b20df2641141afdf9e2cb1f0c0561757b4ebfb54fb6702a68c4334c810#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('9', '1', '1', '10', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000124&idx=1&sn=9621e6c904883347858df72ee3e71ef7&chksm=6c9290165be51900258841e01b9802f690d9c7e188202badcb90f4309d17402e8a4da0d4341e#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('10', '1', '1', '11', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000123&idx=1&sn=79415bfa368d90fcef8f6f84601358f9&chksm=6c9290115be51907767cf81b097b488df8929c85f7cd973a81dc77e05e112c6af6d83a231a31#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('11', '1', '1', '12', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000122&idx=1&sn=9ddbe0a84918cdad5c9f7a9327094cf1&chksm=6c9290105be519064d8f261f8e76b08355c8f0d94890eb11850de205ea23bb16ebbd04dbfdd1#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('12', '1', '1', '13', 'Mkk-XekVsp2Cvr5VktS-vUT0p2_YYCaW_dn84FviKBY', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeyWP9jwjWqpiarU5F2WgdoMGwiccOFXfhJIiciaU8H3jjUmSYbHibP6wEuibXA/0?wx_fmt=png', '1', '1', '1', '121', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000121&idx=1&sn=6a77e91e0e298983a3547ddc8a90c5f9&chksm=6c9290135be51905d324005001683d62e5e54546a5bce6e7c250d6fd1843bfa6c16904720fde#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('13', '1', '1', '14', 'Mkk-XekVsp2Cvr5VktS-vTFi0SiiaAKNOMl-dUwGKWM', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', '测试', '', '1', '123', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000119&idx=1&sn=1e85e2b16befe52f6e71c7f25d1ce009&chksm=6c92901d5be5190b0221b7c1f55734639777a47e264849e6f3171d6ffb495d026ef9ac73ab85#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('14', '1', '1', '15', 'Mkk-XekVsp2Cvr5VktS-vTFi0SiiaAKNOMl-dUwGKWM', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', '测试', '', '1', '123', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000118&idx=1&sn=6ea40d072247c797220cc0f830508b15&chksm=6c92901c5be5190a027707e05930e0919c9e307c687f610f527273b72d6712299b7cc053eefb#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('15', '1', '1', '16', 'Mkk-XekVsp2Cvr5VktS-vTFi0SiiaAKNOMl-dUwGKWM', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', 'http://mmbiz.qpic.cn/mmbiz_png/nKp1y5rQibOa1WWBbKJLyGeQlFbCLBGeymE4MFmib9h6NlBA0Q9puRlQHoibVwZT2ujGdpPqib8JG5xjgia6a40nmCQ/0?wx_fmt=png', '测试', '', '1', '123', '<p>123</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000117&idx=1&sn=d4bebf79034727761925676cb861da3b&chksm=6c92901f5be519099060f29ea3bdda5929f4d64f5f9034162aade7f9b340a5cd26a214cee96d#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('16', '1', '1', '17', 'Mkk-XekVsp2Cvr5VktS-vTHabBENIKUgENJuImMxh_Y', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7phlb1nGCCBOl7H62KYibhyn6ibco65rsP4gldTxiaiaT3EUnXVxynUTtZYw/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7phlb1nGCCBOl7H62KYibhyn6ibco65rsP4gldTxiaiaT3EUnXVxynUTtZYw/0?wx_fmt=jpeg', '123213', '', '1', '12313', '<p>1231</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000102&idx=1&sn=71b2e47398ad2061e7e57d6a69d47af3&chksm=6c92900c5be5191aa0addc23d3d66329a3d5c2f6986eab43c0aa6ddef9072b23abec90d6289e#rd', '', '1', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('17', '1', '1', '18', 'Mkk-XekVsp2Cvr5VktS-vZ8hhRgmG_6atwRl1Uo_iyE', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', '111', '31', '1', '123', '<p>123123213</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000099&idx=1&sn=e6b440d332b5b9edd782ae349c922ca9&chksm=6c9290095be5191fbfd357cba977ce97cb565bb613c3fb703d35965d6b9df9251095b3d0741d#rd', 'http://123', '0', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('18', '1', '1', '19', 'Mkk-XekVsp2Cvr5VktS-vZ8hhRgmG_6atwRl1Uo_iyE', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', '111', '31', '1', '123', '<p>123123213</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000096&idx=1&sn=fda13544a6bb78a9752a974d34892c9a&chksm=6c92900a5be5191cd50fddc9b99b2cf5003653108178e09af214816e0cde4a5475a87e1285fc#rd', 'http://123', '0', '0', '0', '0');
INSERT INTO `hhy_wx_material_info` VALUES ('19', '1', '1', '20', 'Mkk-XekVsp2Cvr5VktS-vZ8hhRgmG_6atwRl1Uo_iyE', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', 'http://mmbiz.qpic.cn/mmbiz_jpg/nKp1y5rQibOaaLXqgpGt60BMaoE6sHv7pVoMZP9cAibmEImZdK0wQEydwAsqMDstLhgcIomCZVrjiacSicia5dFPc9w/0?wx_fmt=jpeg', '111', '31', '1', '123', '<p>123123213</p>', 'http://mp.weixin.qq.com/s?__biz=MzI5OTYxNjI2MQ==&mid=100000095&idx=1&sn=f228fbec4a7dbffc6d0b77bcd63e5bb7&chksm=6c9290355be51923562c72539d96622d80413653e5edec8290d7bf99a5121313aef62a9cab45#rd', 'http://123', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for hhy_wx_menu
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_menu`;
CREATE TABLE `hhy_wx_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `store_id` int(11) NOT NULL DEFAULT '1' COMMENT '店铺id',
  `wx_aid` int(11) DEFAULT NULL,
  `menu_name` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `ico` varchar(32) NOT NULL DEFAULT '' COMMENT '菜图标单',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父菜单',
  `menu_event_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1普通url 2 图文素材 3 功能',
  `media_id` int(11) NOT NULL DEFAULT '0' COMMENT '图文消息ID',
  `menu_event_url` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单url',
  `hits` int(11) NOT NULL DEFAULT '0' COMMENT '触发数',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) DEFAULT '0' COMMENT '创建日期',
  `update_time` int(11) DEFAULT '0' COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `IDX_biz_shop_menu_orders` (`sort`),
  KEY `IDX_biz_shop_menu_shopId` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='微信设置->微信菜单';

-- ----------------------------
-- Records of hhy_wx_menu
-- ----------------------------
INSERT INTO `hhy_wx_menu` VALUES ('1', '0', null, '官网', '', '0', '2', '3', 'http://www.luckyhhy.cn', '0', '1', '1512442512', '0');
INSERT INTO `hhy_wx_menu` VALUES ('2', '0', null, '手册', '', '0', '2', '5', 'http://www.baidu.com', '0', '2', '1512442543', '0');
INSERT INTO `hhy_wx_menu` VALUES ('3', '0', null, '论坛', '', '0', '1', '4', 'http://www.baidu.com', '0', '3', '1512547727', '0');
INSERT INTO `hhy_wx_menu` VALUES ('4', '0', null, '百度', '', '3', '1', '0', 'http://www.baidu.com', '0', '1', '1542783759', '0');

-- ----------------------------
-- Table structure for hhy_wx_msg_history
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_msg_history`;
CREATE TABLE `hhy_wx_msg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(10) unsigned DEFAULT '1' COMMENT '商户id',
  `wx_aid` int(11) DEFAULT NULL COMMENT '微信账号id',
  `media_id` int(11) DEFAULT NULL,
  `keyword_id` int(10) DEFAULT '0' COMMENT '关键字id',
  `nickname` varchar(150) DEFAULT NULL COMMENT '昵称',
  `openid` varchar(50) DEFAULT '',
  `content_json` varchar(1000) DEFAULT NULL,
  `content` varchar(1000) DEFAULT '' COMMENT '微信消息',
  `type` varchar(20) DEFAULT '',
  `event` varchar(20) DEFAULT '' COMMENT '详细事件',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态0:禁用;1启用',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信_历史记录表';

-- ----------------------------
-- Records of hhy_wx_msg_history
-- ----------------------------

-- ----------------------------
-- Table structure for hhy_wx_qrcode
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_qrcode`;
CREATE TABLE `hhy_wx_qrcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `wx_aid` int(11) NOT NULL,
  `name` varchar(50) DEFAULT '',
  `qrcode` varchar(255) NOT NULL,
  `scene_id` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 临时，1 永久',
  `ticket` varchar(255) NOT NULL,
  `expire_seconds` int(11) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT ' ',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='微信二维码';

-- ----------------------------
-- Records of hhy_wx_qrcode
-- ----------------------------
INSERT INTO `hhy_wx_qrcode` VALUES ('1', '1', '1', '测试', 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQFy8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyNVlKYTBQNUNlSTQxS3ZNck51Y0UAAgSfY3ReAwQAjScA', null, '0', 'gQFy8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyNVlKYTBQNUNlSTQxS3ZNck51Y0UAAgSfY3ReAwQAjScA', '2592000', 'http://weixin.qq.com/q/025YJa0P5CeI41KvMrNucE', '1', null, null);

-- ----------------------------
-- Table structure for hhy_wx_reply
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_reply`;
CREATE TABLE `hhy_wx_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '微信关键词回复表',
  `store_id` int(11) NOT NULL DEFAULT '1' COMMENT '店铺id',
  `wx_aid` int(11) DEFAULT NULL,
  `rule` varchar(32) DEFAULT NULL COMMENT '规则名',
  `keyword` varchar(150) DEFAULT NULL,
  `type` varchar(10) DEFAULT 'keyword' COMMENT '查询类型keyword,subscribe,default',
  `msg_type` varchar(10) DEFAULT NULL COMMENT '回复消息类型  文本（text ）图片（image）、视频（video）、语音 （voice）、图文（news） 音乐（music）',
  `data` mediumtext COMMENT 'text使用该自动存储文本',
  `material_id` int(10) unsigned DEFAULT NULL COMMENT 'news、video voice image music的素材id等',
  `status` tinyint(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='微信回复表';

-- ----------------------------
-- Records of hhy_wx_reply
-- ----------------------------
INSERT INTO `hhy_wx_reply` VALUES ('1', '1', '1', null, 'ks', 'keyword', 'text', 'ad撒打算', '0', '1', '1584690587', null);

-- ----------------------------
-- Table structure for hhy_wx_tag
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_tag`;
CREATE TABLE `hhy_wx_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL COMMENT 'tag id',
  `name` varchar(100) NOT NULL COMMENT '标签名',
  `store_id` int(11) NOT NULL DEFAULT '1' COMMENT '店铺id',
  `wx_aid` int(11) DEFAULT NULL COMMENT '微信账号id',
  `status` tinyint(1) DEFAULT '1',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='微信用户标签表';

-- ----------------------------
-- Records of hhy_wx_tag
-- ----------------------------
INSERT INTO `hhy_wx_tag` VALUES ('1', '1', '测试', '1', '1', '1', '1584668886', '0');
INSERT INTO `hhy_wx_tag` VALUES ('2', '105', 'haha', '1', '1', '1', '1584670343', '1584671967');
INSERT INTO `hhy_wx_tag` VALUES ('3', '106', '大萨达撒', '1', '1', '1', '1584694716', '0');

-- ----------------------------
-- Table structure for hhy_wx_type
-- ----------------------------
DROP TABLE IF EXISTS `hhy_wx_type`;
CREATE TABLE `hhy_wx_type` (
  `type_id` tinyint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='微信类型表';

-- ----------------------------
-- Records of hhy_wx_type
-- ----------------------------
INSERT INTO `hhy_wx_type` VALUES ('1', '普通订阅号', '0', '0');
INSERT INTO `hhy_wx_type` VALUES ('2', '认证订阅号', '0', '0');
INSERT INTO `hhy_wx_type` VALUES ('3', '普通服务号', '0', '0');
INSERT INTO `hhy_wx_type` VALUES ('4', '认证服务号/认证媒体/政府订阅号', '0', '0');
