<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2019/11/14 14:42
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------
    namespace app\admin\behavior;

    use app\admin\model\AdminUser;

    class LoginRecord
    {
        public function run($params)
        {
            $user = $params['user'];
            ## 登录记录
            $user['login_at']	= time();
            $user['ip']	= request()->ip();
            $user['logins']=$user['logins']+1;
            AdminUser::update($user);
        }
    }