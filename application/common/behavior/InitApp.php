<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2019/11/14 10:50
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------

    namespace app\common\behavior;

 use think\facade\Env;
class InitApp
{
    /**
     * 执行行为 run方法是Behavior唯一的接口
     * @return void
     */
    public function run()
    {
        // 初始化常量
        $this->initConst();
    }

    /**
     * 初始化常量
     * @return void
     */
    private function initConst()
    {
        // 初始化系统常量
        $this->initSystemConst();
    }

    /**
     * 初始化系统常量
     * @return void
     */
    private function initSystemConst()
    {
        define('THINK_PATH', Env::get('think_path'));
        define('ROOT_PATH', Env::get('root_path'));
        define('APP_PATH', Env::get('app_path'));
        define('CONFIG_PATH', Env::get('config_path'));
        define('ROUTE_PATH', Env::get('route_path'));
        define('RUNTIME_PATH', Env::get('runtime_path'));
        define('EXTEND_PATH', Env::get('extend_path'));
        define('VENDOR_PATH', Env::get('vendor_path'));
        define('PLUGIN_PATH', ROOT_PATH . 'plugins');
        define('PUBLIC', ROOT_PATH . 'public');
        define('ADDON_PATH', ROOT_PATH . 'addons' . DIRECTORY_SEPARATOR);
    }
    

}
