<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-11:18
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\model;


    use app\admin\imple\Comm;
    use app\common\model\BaseModel;
    use Jrk\Trees;
    use think\db\Where;
    use think\Exception;

    class Departments extends BaseModel implements Comm
    {

        protected $name = "co_department";



        /**
         * @param array $param
         * @param string $order
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getAdminPageData
         * @describe:
         */
        public function getAdminPageData($param = [], $order = 'id asc')
        {
            // TODO: Implement getAdminPageData() method.
            $where = [];
            if(!empty($param)) {
                //搜索条件
                if(isset($param['dname']) && $param['dname'] !='') {
                    $where['dname'] =['like', "%".$param['dname']."%"];
                }
            }

            try{
                $data = self::where(new Where($where))->order($order)->page(PAGE)->limit(LIMIT)->select()->toArray();
                $count =self::where(new Where($where))->count("id");

                if (!empty($data)){
                    $data=Trees::toFormatTree($data,"dname");
                }
                $this->ajaxResult($data,0,'',$count);

            }catch (Exception $exception){

                $this->ajaxResult('',100,$exception->getMessage());
            }
        }



        /**
         * @param $data
         * @return mixed|void
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: addAndEdit
         * @describe:
         */
        public function addAndEdit($data)
        {
            // TODO: Implement addAndEdit() method.
           return parent::doAll($data);
        }


        /**
         * @param $id
         * @return mixed|void
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            // TODO: Implement delete() method.
            $res = $this->findChild($id);
            if($res!==true) {
                $this->error("当前部门下有子集，不能删除");
            }
            try {
                $del = $this->destroy(['id' => ['in', $id]]);
                $del?$this->success("删除成功"):$this->error("删除失败");
            } catch (Exception $exception) {
                $this->error($exception->getMessage());
            }

        }


        /**
         * @param $id
         * @return bool
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/1/9
         * @name: findChild
         * @describe:查询是否有子集
         */
        private function findChild($id)
        {
            $res = Trees::getChildrenPid(self::all(), $id);
            if(empty($res)) {
                return true;
            }
            else {
                return false;
            }
        }


        /**
         * @return array
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: deList
         * @describe:
         */
        public function deList(){
           return self::order("id")->where("status",1)->select()->toArray();
        }


        /**
         * @param $id
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/25
         * @name: getOne
         * @describe:
         */
        public function getOne($id){
            return self::find($id);
        }



    }