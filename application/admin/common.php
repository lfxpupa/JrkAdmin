<?php

    use Jrk\Util;
    use think\Exception;
    use think\facade\Env;


    /**
     * @param $pass
     * @return mixed
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: password
     * @describe:密碼加密
     */
    function password($pass){
        return Util::think_encrypt($pass);
    }


    /**
     * @param $pass
     * @return mixed
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: po_pass
     * @describe:解密
     */
    function po_pass($pass){
        return Util::think_decrypt($pass);
    }

    /**
     * @param $data 下拉框数据源
     * @param int $selected_id 选择数据ID
     * @param string $show_field 显示名称
     * @param string $val_field 显示值
     * @author: hhygyl <jackhhy520@qq.com>
     * @name: make_option
     * @describe: 下拉选择框组件
     */
    function make_option($data, $selected_id = 0, $show_field = 'name', $val_field = 'id')
    {
        $html = '';
        $show_field_arr = explode(',', $show_field);
        //dump($data);
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $show_text = '';
                if (is_array($v)) {
                    foreach ($show_field_arr as $s) {
                        $show_text .= $v[$s] . ' ';
                    }
                    $show_text = substr($show_text, 0, -1);
                    $val_field && $k = $v[$val_field];
                } else {
                    $show_text = $v;
                }
                $sel = '';
                if ($selected_id && $k == $selected_id) {
                    $sel = 'selected';
                }
                $html .= '<option value = ' . $k . ' ' . $sel . '>' . $show_text . '</option>';
            }
        }
        echo $html;
    }


    /**
     * @param $data 数据源
     * @param $name select name值
     * @param int $selected_id 选中的值
     * @param string $required 是否必选  空可不选 ，required 必选
     * @param string $filter layui的filter属性
     * @param string $show_field 显示的字段
     * @param string $val_field 值的字段
     * @param string $msg  提示消息
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/23
     * @name: make_options
     * @describe: 数据源转layui-select
     */
    function make_layui_select($data,$name, $selected_id = 0 ,$show_field = 'name', $val_field = 'id',$required="",$filter="",$msg="")
    {
        $html = '<select name="'.$name.'"  lay-verify="'.$required.'" lay-search="" lay-filter="'.$filter.'" lay-verType="tips" lay-reqText="请选择'.$msg.'" ><option value="">【请选择'.$msg.'】</option>';
        $show_field_arr = explode(',', $show_field);
        //dump($data);
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $show_text = '';
                if (is_array($v)) {
                    foreach ($show_field_arr as $s) {
                        $show_text .= $v[$s] . ' ';
                    }
                    $show_text = substr($show_text, 0, -1);
                    $val_field && $k = $v[$val_field];
                } else {
                    $show_text = $v;
                }
                $sel = '';
                if ($selected_id && $k == $selected_id) {
                    $sel = 'selected';
                }
                $html .= '<option value = ' . $k . ' ' . $sel . '>' . $show_text . '</option>';
            }
        }else{
            $html .= '<option value = "0">data不是数组</option>';
        }
        $html.='</select>';
        echo $html;
    }






    /**
     * @param $filename
     * @return string
     * @author: hhygyl <hhygyl520@qq.com>
     * @name: fileext
     * @describe:获取文件后缀名
     */
    function fileext($filename) {
        return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
    }

    /**
     * @param $filepath
     * @param string $filename
     * @author: hhygyl <hhygyl520@qq.com>
     * @name: file_down
     * @describe: 文件下载
     */
    function file_down($filepath, $filename = '') {
        if(!$filename) $filename = basename($filepath);
        if(is_ie()) $filename = rawurlencode($filename);
        $filetype = fileext($filename);
        $filesize = sprintf("%u", filesize($filepath));
        if(ob_get_length() !== false) @ob_end_clean();
        header('Pragma: public');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: pre-check=0, post-check=0, max-age=0');
        header('Content-Transfer-Encoding: binary');
        header('Content-Encoding: none');
        header('Content-type: '.$filetype);
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Content-length: '.$filesize);
        readfile($filepath);
        exit;
    }


    /**
     * @param $content
     * @return mixed|string
     * @author: hhygyl <hhygyl520@qq.com>
     * @name: match_img
     * @describe: 获取内容中的图片
     */
    function match_img($content){
        preg_match('/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/', $content, $match);
        return !empty($match) ? $match[1] : '';
    }


    /**
     * @param $content
     * @param string $targeturl
     * @return bool|mixed
     * @author: hhygyl <hhygyl520@qq.com>
     * @name: grab_image
     * @describe:获取远程图片并把它保存到本地, 确定您有把文件写入本地服务器的权限
     */
    function grab_image($content, $targeturl = ''){
        preg_match_all('/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/', $content, $img_array);
        $img_array = isset($img_array[1]) ? array_unique($img_array[1]) : array();

        if($img_array) {
            $path =  '/uploads/'.'grap_image/'.date('Ym/d');
            $urlpath = Env::get("root_path").$path;
            $imgpath =  Env::get("root_path").$path;
            if(!is_dir($imgpath)) @mkdir($imgpath, 0777, true);
        }

        foreach($img_array as $key=>$value){
            $val = $value;
            if(strpos($value, 'http') === false){
                if(!$targeturl) return $content;
                $value = $targeturl.$value;
            }
            $ext = strrchr($value, '.');
            if($ext!='.png' && $ext!='.jpg' && $ext!='.gif' && $ext!='.jpeg') return false;
            $imgname = date("YmdHis").rand(1,9999).$ext;
            $filename = $imgpath.'/'.$imgname;
            $urlname = $urlpath.'/'.$imgname;

            ob_start();
            readfile($value);
            $data = ob_get_contents();
            ob_end_clean();
            file_put_contents($filename, $data);

            if(is_file($filename)){
                $content = str_replace($val, $urlname, $content);
            }else{
                return $content;
            }
        }
        return $content;
    }










