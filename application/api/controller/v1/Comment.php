<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/12-11:51
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\api\controller\v1;

    //不需登录继承
    use app\api\check\ApiCheck;
    //需登录继承
    use app\api\check\ApiCheckLogin;
    /**
     * Class Comment
     * @package app\api\controller\v1
     */
    class Comment extends ApiCheckLogin
    {
        /**
         * @return \think\response\Json
         * @throws \Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/12
         * @name: getComment
         * @describe:
         */
        public function  getComment(){
            //查询数据
            $wechat_fans=db("wechat_fans")->paginate(15);

            return ajaxReturn(ERRNO['OK'],'查询数据成功',$wechat_fans);

        }

    }