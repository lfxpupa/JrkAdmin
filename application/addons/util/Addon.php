<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/18-11:06
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\addons\util;

    use think\Db;
    use think\facade\Config;
    use think\View;

    /**
     * Class Addon
     * @package app\addons\util
     */
    abstract class Addon
    {

        /**
         * 视图实例对象
         * @var view
         * @access protected
         */
        protected $view = null;
        /**
         * @var array
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $info = [];
        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $addon_path = '';
        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $config_file = '';
        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $custom_config = '';
        /**
         * @var array
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $admin_list = [];
        /**
         * @var string
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $custom_adminlist = '';
        /**
         * @var array
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18-11:09
         */
        public $access_url = [];

        /**
         * Addon constructor.
         */
        public function __construct()
        {
            // 获取当前插件目录
            $this->addon_path = ADDON_PATH.$this->getName().DIRECTORY_SEPARATOR;
            // 读取当前插件配置信息
            if (is_file($this->addon_path.'config.php')) {
                $this->config_file = $this->addon_path.'config.php';
            }
            // 初始化视图模型
            $config['view_path'] = $this->addon_path;
            $config              = array_merge(Config::get('template.'), $config);
            $this->view          = new View();
            $this->view          = $this->view->init($config);
            //加载插件函数文件
            if (file_exists($this->addon_path.'common.php')) {
                include_once $this->addon_path.'common.php';
            }
        }

        /**
         * @param $name
         * @param string $value
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: assign
         * @describe: 模板变量赋值
         */
        final protected function assign($name, $value = '')
        {
            $this->view->assign($name, $value);
        }

        /**
         * @param string $template
         * @param array $vars
         * @param array $replace
         * @param array $config
         * @throws \Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: fetch
         * @describe:加载模板和页面输出 可以返回输出内容
         */
        public function fetch($template = '', $vars = [], $replace = [], $config = [])
        {
            if (!is_file($template)) {
                $template = '/'.$template;
            }
            // 关闭模板布局
            $this->view->engine->layout(false);

            echo $this->view->fetch($template, $vars, $replace, $config);
        }


        /**
         * @param $content
         * @param array $vars
         * @param array $replace
         * @param array $config
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: display
         * @describe:渲染内容输出
         */
        public function display($content, $vars = [], $replace = [], $config = [])
        {
            // 关闭模板布局
            $this->view->engine->layout(false);

            echo $this->view->display($content, $vars, $replace, $config);
        }



        /**
         * @param $content
         * @param array $vars
         * @throws \Exception
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: show
         * @describe: 渲染内容输出
         */
        public function show($content, $vars = [])
        {
            // 关闭模板布局
            $this->view->engine->layout(false);

            echo $this->view->fetch($content, $vars, [], [], true);
        }



        /**
         * @title 获取当前模块名
         * @return string
         */
        final public function getName()
        {
            $data = explode('\\', get_class($this));

            return strtolower(array_pop($data));
        }



        /**
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: checkInfo
         * @describe:获取当前模块名
         */
        final public function checkInfo()
        {
            $info_check_keys = ['name', 'title', 'description', 'status', 'author', 'version'];
            foreach ($info_check_keys as $value) {
                if (!array_key_exists($value, $this->info)) {
                    return false;
                }

            }

            return true;
        }



        /**
         * @param string $name
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: getAddonConfig
         * @describe:获取插件的配置数组
         */
        final public function getAddonConfig($name = '')
        {
            static $_config = [];
            if (empty($name)) {
                $name = $this->getName();
            }
            if (isset($_config[$name])) {
                return $_config[$name];
            }
            $map['name']   = $name;
            $map['status'] = 1;
            $config        = Db::name('Addons')->where($map)->value('config');
            if ($config) {
                $config = json_decode($config, true);
            } else {
                if (is_file($this->config_file)) {
                    $temp_arr = include $this->config_file;
                    foreach ($temp_arr as $key => $value) {
                        if ($value['type'] == 'group') {
                            foreach ($value['options'] as $gkey => $gvalue) {
                                foreach ($gvalue['options'] as $ikey => $ivalue) {
                                    $config[$ikey] = $ivalue['value'];
                                }
                            }
                        } else {
                            $config[$key] = $temp_arr[$key]['value'];
                        }
                    }
                    unset($temp_arr);
                }
            }
            $_config[$name] = $config;

            return $config;
        }


        /**
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: install
         * @describe:必须实现安装
         */
        abstract public function install();


        /**
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/18
         * @name: uninstall
         * @describe:必须卸载插件方法
         */
        abstract public function uninstall();

    }