<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/18-10:08
    // +----------------------------------------------------------------------
    // | Description:  插件函数
    // +----------------------------------------------------------------------

    /**
     * @param string $name
     * @param string $controller
     * @param string $action
     * @return bool
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/18
     * @name: pluginActionExists
     * @describe:
     */
    function pluginActionExists($name = '', $controller = '', $action = '')
    {
        if (strpos($name, '/')) {
            list($name, $controller, $action) = explode('/', $name);
        }
        return method_exists("addons\\{$name}\\controller\\{$controller}", $action);
    }