<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-15:03
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class StationValidate extends BaseValidate
    {

        protected $rule = [
            'd_id' =>  'checkId',
            'sname'  =>  'chsAlpha|max:20',

        ];

        protected $message = [
            'sname.chsAlpha'  =>  '岗位名称只允许汉字、字母',
            'sname.max'  =>  '岗位名称最大长度为20个字符',
        ];



        protected function checkId($value,$rule,$data){
            if($value < 1){
                return '部门ID错误';
            }
            return true;
        }

    }