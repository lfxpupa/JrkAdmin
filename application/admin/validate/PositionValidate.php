<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/25-15:26
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class PositionValidate extends BaseValidate
    {


        protected $rule = [
            'pname|token'  =>  'chsAlpha|max:20',
        ];

        protected $message = [
            'pname.chsAlpha'  =>  '职级名称只允许汉字、字母',
            'pname.max'  =>  '职级名称最大长度为20个字符',
        ];

    }