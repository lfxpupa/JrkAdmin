<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/19-10:29
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\wechat\service;

    use app\wechat\model\WxAccount;
    use EasyWeChat\Factory;

    /**
     * Class WechatApp
     * @package app\wechat\service
     */
    class WechatApp
    {
        /**
         * @var array
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19-10:30
         */
        protected $config = [
            'log'   => [
                'default'  => 'dev', // 默认使用的 channel，生产环境可以改为下面的 prod
                'channels' => [
                    // 测试环境
                    'dev'  => [
                        'driver' => 'single',
                        'path'   => '../runtime/easywechat.log',
                        'level'  => 'debug',
                    ],
                    // 生产环境
                    'prod' => [
                        'driver' => 'daily',
                        'path'   => '../runtime/easywechat.log',
                        'level'  => 'info',
                    ],
                ],
            ],
            'http'  => [
                'max_retries' => 1,
                'retry_delay' => 500,
                'timeout'     => 5.0,
                // 'base_uri' => 'https://api.weixin.qq.com/', // 如果你在国外想要覆盖默认的 url 的时候才使用，根据不同的模块配置不同的 uri
            ],
            /**
             * OAuth 配置
             * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
             * callback：OAuth授权完成后的回调页地址
             */
            'oauth' => [
                'scopes'   => ['snsapi_userinfo'],
                'callback' => '/examples/oauth_callback.php', //回掉地址
            ],
        ];
        /**
         * @var array|\PDOStatement|string|\think\Model|null
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19-10:30
         */
        public $wechat;
        /**
         * @var int
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19-10:30
         */
        public $store_id;
        /**
         * @var \EasyWeChat\OfficialAccount\Application
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @date: 2020/3/19-10:30
         */
        public $wechatApp;

        /**
         * WechatApp constructor.
         * @param int $store_id
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         */
        public function __construct($store_id = 1)
        {
            // 微信配置
            $this->store_id = $store_id;
            $this->wechat   = WxAccount::where('status', 1)->where('store_id', $this->store_id)->find();
            if ($this->wechat) {
                $this->config    = [
                    'app_id'        => $this->wechat->app_id,
                    'secret'        => $this->wechat->app_secret,
                    'token'         => $this->wechat->w_token,
                    'response_type' => 'array',
                ];
                $this->wechatApp = cache('wechatapp_'.$this->wechat->id);
                if (!$this->wechatApp) {
                    $this->wechatApp = Factory::officialAccount($this->config);
                }

            } else {

                return false;
            }
        }

    }