<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2019/11/14 10:50
    // +----------------------------------------------------------------------
    // | Description:
    // +----------------------------------------------------------------------

    namespace app\http\middleware;


    class CheckLogin
    {
        public function handle($request, \Closure $next)
        {
            if (!$request->session(config('app.user_info_session'))) {
                //dump(2);
                return redirect(url("/admin/login/index"));
            }
            return $next($request);
        }
    }
