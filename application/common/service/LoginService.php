<?php

    namespace app\common\service;

    use think\facade\Cookie;
    use think\facade\Session;

    class LoginService
    {

        /**
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @name: getAdminUserInfo
         * @describe:获取登录保存的session信息
         */
        public static function getAdminUserInfo()
        {
            $userinfo=Session::get(config("app.user_info_session"));
            return $userinfo;
        }


        /**
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @name: loginOut
         * @describe:退出登录，删除登录session信息
         */
        public static function loginOut()
        {
            Session::delete(config("app.user_info_session"));
            return true;
        }

        /**
         * @param null $user
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @name: storeAdminInfo
         * @describe:保存用户信息
         */
        public static function storeAdminInfo($user=null)
        {
            Session::set(config('app.user_info_session'), $user);
        }

        /**
         * @param null $data
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @name: adminRemember
         * @describe:记住用户登录
         */
        public static function adminRemember($data=null)
        {
            Cookie::set("admin_user", $data);
        }


        /**
         * @return mixed
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @name: getRemember
         * @describe:获取记住的cookie
         */
        public static function getRemember()
        {
            $user= Cookie::get("admin_user");
            return $user;
        }

        /**
         * @return bool
         * @author: LuckyHhy <jackhhy520@qq.com>
         * @name: delRemember
         * @describe:删除记录的cookie
         */
        public static function delRemember()
        {
            Cookie::delete("admin_user");
            return true;
        }
    }
