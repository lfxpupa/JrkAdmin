<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/2/26-15:57
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class Pass extends BaseValidate
    {

        protected $rule = [
            'oldpassword|token'  =>  'alphaNum',
            'password' =>  'min:6|max:16|alphaNum',
            'repassword' =>  'confirm:password',
        ];

        protected $message = [
            'oldpassword.alphaNum'  =>  '密码只允许字母、数字',
            'password.min'  =>  '密码最长16个字符',
            'password.max'  =>  '密码最长16个字符',
            'password.alphaNum'  =>  '密码只允许字母、数字',

        ];

    }