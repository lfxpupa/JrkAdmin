<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2020/3/21 0021-15:46
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------

namespace app\addons\validate;


use think\Validate;

class Hooks extends Validate
{

    protected $rule = [
        'name'  =>  'require|max:100|chsAlphaNum|token',
    ];

    protected $message = [
        'name.require'  =>  '钩子名称必填',
        'name.max'  =>  '钩子名称最多100个字符',
        'name.chsAlphaNum'  =>  'attribute must be chinese,alpha-numeric',
    ];


}