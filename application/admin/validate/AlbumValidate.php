<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/10-15:17
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class AlbumValidate extends BaseValidate
    {

        protected $rule = [
            'category_id' =>  'checkId',
            'name'  =>  'require|max:100|token',
            'password'          => 'max:6',

        ];

        protected $message = [
            'name.require'  =>  '相册名称为必填',
            'name.max'  =>  '标题最多100个字符',
            'password.max' =>  '密码最多6位',

        ];

        protected function checkId($value,$rule,$data){
            if($value < 1){
                return '栏目ID错误';
            }
            return true;
        }


    }