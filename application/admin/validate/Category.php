<?php
    // +----------------------------------------------------------------------
    // | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ] 
    // +----------------------------------------------------------------------
    // | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
    // +----------------------------------------------------------------------
    // | SiteUrl: http://www.luckyhhy.cn
    // +----------------------------------------------------------------------
    // | Author: LuckyHhy <jackhhy520@qq.com>
    // +----------------------------------------------------------------------
    // | Date: 2020/3/4-14:34
    // +----------------------------------------------------------------------
    // | Description:  
    // +----------------------------------------------------------------------


    namespace app\admin\validate;


    use app\common\validate\BaseValidate;

    class Category extends BaseValidate
    {

        protected $rule = [
            'model_id' =>  'checkId',
            'name'  =>  'require|max:150',
            'description' =>  'max:200',
        ];

        protected $message = [
            'name.require'  =>  '栏目名称为必填',
            'name.max'  =>  '栏目名称最多150个字符',
            'description.max' =>  '栏目描述最多200个字符',
        ];



        protected function checkId($value,$rule,$data){
            if($value < 1){
                return '模型ID错误';
            }
            return true;
        }


    }