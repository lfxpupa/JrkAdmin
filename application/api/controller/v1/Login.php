<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: JRKAdmin框架 [ JRKAdmin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2020/3/14 0014-17:10
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------

namespace app\api\controller\v1;


use app\api\check\ApiCheck;

class Login extends ApiCheck
{

    /**
     * @return \think\response\Json
     * @throws \Exception
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/3/14 0014
     * @name: login
     * @describe:用户登录接口请求验证
     */
    public function login(){
        //接受登录传过来的信息
        $param=$this->request->param();

        //1、验证登录信息

        // 密码账号等，假如通过

        //查询 用户所有信息

        $userInfo=['uid='=>1,"username"=>'jackhhy','password'=>'123456'];

        //返回用户信息 给与前端保存
        return ajaxReturn(setJWT($userInfo));

    }

}